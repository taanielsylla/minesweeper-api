﻿using System;

namespace Enums
{
    public static class RoundStatusEnum
    {
        public static readonly string NoGame = "NO_GAME";
        public static readonly string Active = "ACTIVE";
        public static readonly string Resolved = "RESOLVED";

        public static readonly Guid NoGameGuid = new Guid("00000000-0000-0000-0000-000000000001");
        public static readonly Guid ActiveGuid = new Guid("00000000-0000-0000-0000-000000000002");
        public static readonly Guid ResolvedGuid = new Guid("00000000-0000-0000-0000-000000000003");
    }
}