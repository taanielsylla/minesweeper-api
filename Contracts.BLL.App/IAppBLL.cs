﻿using System;
using Contracts.BLL.App.Services;
using ee.itcollege.tasull.Contracts.BLL.@base;

namespace Contracts.BLL.App
{
    public interface IAppBLL : IBaseBLL
    {
        ILangStrService LangStrs { get; }
        ILangStrTranslationService LangStrTranslation { get; }

        IAffiliateService Affiliates { get; }
        IConfigurationService Configurations { get; }
        ICurrencyTypeService CurrencyTypes { get; }
        IFaucetService Faucets { get; }
        IGameHistoryService GameHistories { get; }
        IGameLevelService GameLevels { get; }
        IGameModeService GameModes { get; }
        IGameRoundService GameRounds { get; }
        IRoundStatusService RoundStatuses { get; }
        ITransactionService Transactions { get; }
        ITransactionStatusService TransactionStatuses { get; }
        ITransactionTypeService TransactionTypes { get; }
        IWalletService Wallets { get; }
    }
}