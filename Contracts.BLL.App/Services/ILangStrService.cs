using Contracts.DAL.App.Repositories;
using BLL.App.DTO;
using ee.itcollege.tasull.Contracts.BLL.@base.Services;

namespace Contracts.BLL.App.Services
{
    public interface ILangStrService : IBaseEntityService<LangStr>, ILangStrRepositoryCustom<LangStr>
    {
        
    }
}