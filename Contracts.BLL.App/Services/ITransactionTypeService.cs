using BLL.App.DTO;
using ee.itcollege.tasull.Contracts.BLL.@base.Services;
using Contracts.DAL.App.Repositories;

namespace Contracts.BLL.App.Services
{
    public interface ITransactionTypeService : IBaseEntityService<TransactionType>, ITransactionTypeRepositoryCustom<TransactionType>
    {
    }
}