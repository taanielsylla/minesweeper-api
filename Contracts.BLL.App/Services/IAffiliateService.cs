using BLL.App.DTO;
using ee.itcollege.tasull.Contracts.BLL.@base.Services;
using Contracts.DAL.App.Repositories;

namespace Contracts.BLL.App.Services
{
    public interface IAffiliateService : IBaseEntityService<Affiliate>, IAffiliateRepositoryCustom<Affiliate>
    {
    }
}