using BLL.App.DTO;
using BLL.App.DTO.Views;
using ee.itcollege.tasull.Contracts.BLL.@base.Services;
using Contracts.DAL.App.Repositories;

namespace Contracts.BLL.App.Services
{
    public interface IConfigurationService : IBaseEntityService<Configuration>, IConfigurationRepositoryCustom<ConfigurationView>
    {
    }
}