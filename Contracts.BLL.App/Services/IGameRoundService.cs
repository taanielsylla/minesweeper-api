using System;
using System.Threading.Tasks;
using ee.itcollege.tasull.Contracts.BLL.@base.Services;
using Contracts.DAL.App.Repositories;
using PublicApi.DTO.v1.Views;
using GameRound = BLL.App.DTO.GameRound;
using StateView = BLL.App.DTO.Views.StateView;
using UserStateView = BLL.App.DTO.Views.UserStateView;
using V1DTO = PublicApi.DTO.v1;

namespace Contracts.BLL.App.Services
{
    public interface IGameRoundService : IBaseEntityService<GameRound>, IGameRoundRepositoryCustom<GameRound>
    {
        Task<StateView> GetStateViewAsync();
        Task<UserStateView?> GetUserStateViewAsync(Guid userId);
        Task<UserStateView?> InitRound(InitRequestView initRequest, Guid userId);
        Task<UserStateView?> ExecuteBet(Guid userId);
        Task<UserStateView?> ResolveRound(Guid userId);
    }
}