using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.App.DTO;
using BLL.App.DTO.Views;
using ee.itcollege.tasull.Contracts.BLL.@base.Services;
using Contracts.DAL.App.Repositories;

namespace Contracts.BLL.App.Services
{
    public interface IWalletService : IBaseEntityService<Wallet>, IWalletRepositoryCustom<Wallet>
    {
        Task CreateNewUserWallets(Guid userId);
        Task<List<UserWalletView>> GetUserWallets(Guid userId);
    }
}