using ee.itcollege.tasull.Contracts.BLL.@base.Mappers;
using BLLAppDTO=BLL.App.DTO;
using DALAppDTO=DAL.App.DTO;

namespace Contracts.BLL.App.Mappers
{
    public interface IConfigurationServiceMapper: IBaseMapper<DALAppDTO.Configuration, BLLAppDTO.Configuration>
    {
        BLLAppDTO.Views.ConfigurationView MapConfigurationView(DALAppDTO.Views.ConfigurationView inObject);
    }
}