using ee.itcollege.tasull.Contracts.BLL.@base.Mappers;
using BLLAppDTO=BLL.App.DTO;
using DALAppDTO=DAL.App.DTO;

namespace Contracts.BLL.App.Mappers
{
    public interface IGameModeServiceMapper: IBaseMapper<DALAppDTO.GameMode, BLLAppDTO.GameMode>
    {
        
    }
}