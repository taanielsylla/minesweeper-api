using ee.itcollege.tasull.Contracts.BLL.@base.Mappers;
using BLLAppDTO = BLL.App.DTO;
using DALAppDTO = DAL.App.DTO;

namespace Contracts.BLL.App.Mappers
{
    public interface IGameRoundServiceMapper : IBaseMapper<DALAppDTO.GameRound, BLLAppDTO.GameRound>
    {
        BLLAppDTO.Views.ConfigurationView MapConfigurationView(DALAppDTO.Views.ConfigurationView inObject);
        BLLAppDTO.Views.HistoryView MapHistoryView(DALAppDTO.Views.HistoryView inObject);
        BLLAppDTO.Views.UserWalletView MapUserWalletView(DALAppDTO.Views.UserWalletView inObject);
    }
}