using ee.itcollege.tasull.Contracts.BLL.@base.Mappers;
using BLLAppDTO=BLL.App.DTO;
using DALAppDTO=DAL.App.DTO;

namespace Contracts.BLL.App.Mappers
{
    public interface IWalletServiceMapper: IBaseMapper<DALAppDTO.Wallet, BLLAppDTO.Wallet>
    {
        BLLAppDTO.Views.UserWalletView MapUserWalletView(DALAppDTO.Views.UserWalletView inObject);
    }
}