﻿using BLL.App.Services;
using ee.itcollege.tasull.BLL.@base;
using Contracts.BLL.App;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;

namespace BLL.App
{
    public class AppBLL : BaseBLL<IAppUnitOfWork>, IAppBLL
    {
        public AppBLL(IAppUnitOfWork uow) : base(uow)
        {
        }

        public ILangStrService LangStrs =>
            GetService<ILangStrService>(() => new LangStrService(UOW));

        public ILangStrTranslationService LangStrTranslation =>
            GetService<ILangStrTranslationService>(() => new LangStrTranslationService(UOW));

        public IAffiliateService Affiliates =>
            GetService<IAffiliateService>(() => new AffiliateService(UOW));

        public IConfigurationService Configurations =>
            GetService<IConfigurationService>(() => new ConfigurationService(UOW));

        public ICurrencyTypeService CurrencyTypes =>
            GetService<ICurrencyTypeService>(() => new CurrencyTypeService(UOW));

        public IFaucetService Faucets =>
            GetService<IFaucetService>(() => new FaucetService(UOW));

        public IGameHistoryService GameHistories =>
            GetService<IGameHistoryService>(() => new GameHistoryService(UOW));

        public IGameLevelService GameLevels =>
            GetService<IGameLevelService>(() => new GameLevelService(UOW));

        public IGameModeService GameModes =>
            GetService<IGameModeService>(() => new GameModeService(UOW));

        public IGameRoundService GameRounds =>
            GetService<IGameRoundService>(() => new GameRoundService(UOW));

        public IRoundStatusService RoundStatuses =>
            GetService<IRoundStatusService>(() => new RoundStatusService(UOW));

        public ITransactionService Transactions =>
            GetService<ITransactionService>(() => new TransactionService(UOW));

        public ITransactionStatusService TransactionStatuses =>
            GetService<ITransactionStatusService>(() => new TransactionStatusService(UOW));

        public ITransactionTypeService TransactionTypes =>
            GetService<ITransactionTypeService>(() => new TransactionTypeService(UOW));

        public IWalletService Wallets =>
            GetService<IWalletService>(() => new WalletService(UOW));
    }
}