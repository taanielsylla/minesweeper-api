using AutoMapper;
using BLL.App.DTO.Views;
using Contracts.BLL.App.Mappers;
using BLLAppDTO = BLL.App.DTO;
using DALAppDTO = DAL.App.DTO;

namespace BLL.App.Mappers
{
    public class GameRoundServiceMapper : BLLMapper<DALAppDTO.GameRound, BLLAppDTO.GameRound>, IGameRoundServiceMapper
    {
        public GameRoundServiceMapper() : base()
        {
            MapperConfigurationExpression
                .CreateMap<DALAppDTO.Views.ConfigurationView, BLLAppDTO.Views.ConfigurationView>();

            // add more mappings
            MapperConfigurationExpression.CreateMap<DALAppDTO.Identity.AppUser, BLLAppDTO.Identity.AppUser>();
            MapperConfigurationExpression.CreateMap<DALAppDTO.Views.HistoryView, BLLAppDTO.Views.HistoryView>();

            MapperConfigurationExpression.CreateMap<DALAppDTO.Views.UserWalletView, BLLAppDTO.Views.UserWalletView>();
            MapperConfigurationExpression.CreateMap<BLLAppDTO.Views.UserWalletView, DALAppDTO.Views.UserWalletView>();


            Mapper = new Mapper(new MapperConfiguration(MapperConfigurationExpression));
        }

        public BLLAppDTO.Views.ConfigurationView MapConfigurationView(DALAppDTO.Views.ConfigurationView inObject)
        {
            return Mapper.Map<BLLAppDTO.Views.ConfigurationView>(inObject);
        }

        public BLLAppDTO.Views.HistoryView MapHistoryView(DALAppDTO.Views.HistoryView inObject)
        {
            return Mapper.Map<BLLAppDTO.Views.HistoryView>(inObject);
        }

        public BLLAppDTO.Views.UserWalletView MapUserWalletView(DAL.App.DTO.Views.UserWalletView inObject)
        {
            return Mapper.Map<BLLAppDTO.Views.UserWalletView>(inObject);
        }
    }
}