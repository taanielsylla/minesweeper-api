using AutoMapper;
using Contracts.BLL.App.Mappers;
using BLLAppDTO = BLL.App.DTO;
using DALAppDTO = DAL.App.DTO;

namespace BLL.App.Mappers
{
    public class WalletServiceMapper : BLLMapper<DALAppDTO.Wallet, BLLAppDTO.Wallet>, IWalletServiceMapper
    {
        public WalletServiceMapper() : base()
        {
            // add more mappings
            MapperConfigurationExpression.CreateMap<DALAppDTO.Views.UserWalletView, BLLAppDTO.Views.UserWalletView>();
            MapperConfigurationExpression.CreateMap<BLLAppDTO.Views.UserWalletView, DALAppDTO.Views.UserWalletView>();

            Mapper = new Mapper(new MapperConfiguration(MapperConfigurationExpression));
        }

        public BLLAppDTO.Views.UserWalletView MapUserWalletView(DALAppDTO.Views.UserWalletView inObject)
        {
            return Mapper.Map<BLLAppDTO.Views.UserWalletView>(inObject);
        }
    }
}