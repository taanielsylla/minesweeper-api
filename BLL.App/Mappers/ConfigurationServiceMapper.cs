using AutoMapper;
using Contracts.BLL.App.Mappers;
using BLLAppDTO = BLL.App.DTO;
using DALAppDTO = DAL.App.DTO;

namespace BLL.App.Mappers
{
    public class ConfigurationServiceMapper : BLLMapper<DALAppDTO.Configuration, BLLAppDTO.Configuration>,
        IConfigurationServiceMapper
    {
        public ConfigurationServiceMapper() : base()
        {
            MapperConfigurationExpression
                .CreateMap<DALAppDTO.Views.ConfigurationView, BLLAppDTO.Views.ConfigurationView>();
            // add more mappings
            MapperConfigurationExpression.CreateMap<DALAppDTO.Identity.AppUser, BLLAppDTO.Identity.AppUser>();

            Mapper = new Mapper(new MapperConfiguration(MapperConfigurationExpression));
        }

        public BLLAppDTO.Views.ConfigurationView MapConfigurationView(DALAppDTO.Views.ConfigurationView inObject)
        {
            return Mapper.Map<BLLAppDTO.Views.ConfigurationView>(inObject);
        }
    }
}