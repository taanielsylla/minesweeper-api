using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.DTO.Views;
using BLL.App.Mappers;
using ee.itcollege.tasull.BLL.@base.Services;
using Contracts.BLL.App.Mappers;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;

namespace BLL.App.Services
{
    public class WalletService :
        BaseEntityService<IAppUnitOfWork, IWalletRepository, IWalletServiceMapper,
            DAL.App.DTO.Wallet, BLL.App.DTO.Wallet>, IWalletService
    {
        public WalletService(IAppUnitOfWork uow) : base(uow, uow.Wallets,
            new WalletServiceMapper())
        {
        }

        public async Task CreateNewUserWallets(Guid userId)
        {
            UOW.Wallets.Add(new DAL.App.DTO.Wallet() // BTC wallet
            {
                AppUserId = userId,
                CurrencyTypeId = new Guid("00000000-0000-0000-0000-000000000001"),
                Amount = 777.0,
            });

            UOW.Wallets.Add(new DAL.App.DTO.Wallet() // mBTC wallet
            {
                AppUserId = userId,
                CurrencyTypeId = new Guid("00000000-0000-0000-0000-000000000002"),
                Amount = 777.0,
            });

            UOW.Wallets.Add(new DAL.App.DTO.Wallet() // Satoshi wallet
            {
                AppUserId = userId,
                CurrencyTypeId = new Guid("00000000-0000-0000-0000-000000000001"),
                Amount = 777.0,
            });

            await UOW.SaveChangesAsync();
        }

        public async Task<List<UserWalletView>> GetUserWallets(Guid userId)
        {
            var dalWallets = await UOW.Wallets.GetUserWallets(userId);
            var bllWallets = dalWallets.Select(dalWallet => Mapper.MapUserWalletView(dalWallet)).ToList();

            return bllWallets;
        }
    }
}