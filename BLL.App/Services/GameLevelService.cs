using BLL.App.Mappers;
using ee.itcollege.tasull.BLL.@base.Services;
using Contracts.BLL.App.Mappers;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;

namespace BLL.App.Services
{
    public class GameLevelService :
        BaseEntityService<IAppUnitOfWork, IGameLevelRepository, IGameLevelServiceMapper,
            DAL.App.DTO.GameLevel, BLL.App.DTO.GameLevel>, IGameLevelService
    {
        public GameLevelService(IAppUnitOfWork uow) : base(uow, uow.GameLevels,
            new GameLevelServiceMapper())
        {
        }
    }
}