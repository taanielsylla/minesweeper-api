using BLL.App.Mappers;
using ee.itcollege.tasull.BLL.@base.Services;
using Contracts.BLL.App.Mappers;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;

namespace BLL.App.Services
{
    public class TransactionService :
        BaseEntityService<IAppUnitOfWork, ITransactionRepository, ITransactionServiceMapper,
            DAL.App.DTO.Transaction, BLL.App.DTO.Transaction>, ITransactionService
    {
        public TransactionService(IAppUnitOfWork uow) : base(uow, uow.Transactions,
            new TransactionServiceMapper())
        {
        }
    }
}