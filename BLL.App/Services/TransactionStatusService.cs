using BLL.App.Mappers;
using ee.itcollege.tasull.BLL.@base.Services;
using Contracts.BLL.App.Mappers;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;

namespace BLL.App.Services
{
    public class TransactionStatusService :
        BaseEntityService<IAppUnitOfWork, ITransactionStatusRepository, ITransactionStatusServiceMapper,
            DAL.App.DTO.TransactionStatus, BLL.App.DTO.TransactionStatus>, ITransactionStatusService
    {
        public TransactionStatusService(IAppUnitOfWork uow) : base(uow, uow.TransactionStatuses,
            new TransactionStatusServiceMapper())
        {
        }
    }
}