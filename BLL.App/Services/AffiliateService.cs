using BLL.App.Mappers;
using ee.itcollege.tasull.BLL.@base.Services;
using Contracts.BLL.App.Mappers;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;

namespace BLL.App.Services
{
    public class AffiliateService :
        BaseEntityService<IAppUnitOfWork, IAffiliateRepository, IAffiliateServiceMapper,
            DAL.App.DTO.Affiliate, BLL.App.DTO.Affiliate>, IAffiliateService
    {
        public AffiliateService(IAppUnitOfWork uow) : base(uow, uow.Affiliates,
            new AffiliateServiceMapper())
        {
        }
    }
}