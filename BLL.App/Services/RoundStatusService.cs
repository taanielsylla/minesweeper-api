using BLL.App.Mappers;
using ee.itcollege.tasull.BLL.@base.Services;
using Contracts.BLL.App.Mappers;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;

namespace BLL.App.Services
{
    public class RoundStatusService :
        BaseEntityService<IAppUnitOfWork, IRoundStatusRepository, IRoundStatusServiceMapper,
            DAL.App.DTO.RoundStatus, BLL.App.DTO.RoundStatus>, IRoundStatusService
    {
        public RoundStatusService(IAppUnitOfWork uow) : base(uow, uow.RoundStatuses,
            new RoundStatusServiceMapper())
        {
        }
    }
}