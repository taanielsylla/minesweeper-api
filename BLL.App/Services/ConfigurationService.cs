using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.DTO.Views;
using BLL.App.Mappers;
using ee.itcollege.tasull.BLL.@base.Services;
using Contracts.BLL.App.Mappers;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;

namespace BLL.App.Services
{
    public class ConfigurationService :
        BaseEntityService<IAppUnitOfWork, IConfigurationRepository, IConfigurationServiceMapper,
            DAL.App.DTO.Configuration, BLL.App.DTO.Configuration>, IConfigurationService
    {
        public ConfigurationService(IAppUnitOfWork uow) : base(uow, uow.Configurations,
            new ConfigurationServiceMapper())
        {
        }

        public virtual async Task<IEnumerable<ConfigurationView>> GetAllForViewAsync()
        {
            var result = (await Repository.GetAllForViewAsync())
                .Select(e => Mapper.MapConfigurationView(e));

            return result;
        }
    }
}