using BLL.App.Mappers;
using ee.itcollege.tasull.BLL.@base.Services;
using Contracts.BLL.App.Mappers;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;

namespace BLL.App.Services
{
    public class CurrencyTypeService :
        BaseEntityService<IAppUnitOfWork, ICurrencyTypeRepository, ICurrencyTypeServiceMapper,
            DAL.App.DTO.CurrencyType, BLL.App.DTO.CurrencyType>, ICurrencyTypeService
    {
        public CurrencyTypeService(IAppUnitOfWork uow) : base(uow, uow.CurrencyTypes,
            new CurrencyTypeServiceMapper())
        {
        }
    }
}