using BLL.App.Mappers;
using ee.itcollege.tasull.BLL.@base.Services;
using Contracts.BLL.App.Mappers;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;

namespace BLL.App.Services
{
    public class GameModeService :
        BaseEntityService<IAppUnitOfWork, IGameModeRepository, IGameModeServiceMapper,
            DAL.App.DTO.GameMode, BLL.App.DTO.GameMode>, IGameModeService
    {
        public GameModeService(IAppUnitOfWork uow) : base(uow, uow.GameModes,
            new GameModeServiceMapper())
        {
        }
    }
}