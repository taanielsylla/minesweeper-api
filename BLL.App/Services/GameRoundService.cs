using System;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.Mappers;
using ee.itcollege.tasull.BLL.@base.Services;
using Contracts.BLL.App.Mappers;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using PublicApi.DTO.v1.Views;
using GameRound = DAL.App.DTO.GameRound;
using StateView = BLL.App.DTO.Views.StateView;
using UserStateView = BLL.App.DTO.Views.UserStateView;
using Wallet = DAL.App.DTO.Wallet;

namespace BLL.App.Services
{
    public class GameRoundService :
        BaseEntityService<IAppUnitOfWork, IGameRoundRepository, IGameRoundServiceMapper, DAL.App.DTO.GameRound,
            BLL.App.DTO.GameRound>, IGameRoundService
    {
        public GameRoundService(IAppUnitOfWork uow) : base(uow, uow.GameRounds, new GameRoundServiceMapper())
        {
        }

        //************************************************************************************************ GAMEDATA 
        /// <summary>
        /// Returns data about game currencies, configurations and history entries for a non logged in user.
        /// </summary>
        /// <returns>StateView object</returns>
        public virtual async Task<StateView> GetStateViewAsync()
        {
            var supportedGameModes = await UOW.GameModes.GetAllAsync();
            var supportedCurrencies = await UOW.CurrencyTypes.GetAllAsync();
            var configs = await UOW.Configurations.GetAllForViewAsync();
            var lastHistory = await UOW.GameHistories.GetLastNEntries(10);

            var stateView = new StateView()
            {
                GameModes = supportedGameModes.Select(gameMode => gameMode.Name).ToList(),
                Currencies = supportedCurrencies.Select(currency => currency.Name).ToList(),
                Configurations = configs.Select(dalConfigView => Mapper.MapConfigurationView(dalConfigView)).ToList(),
                GameHistory = lastHistory.Select(dalHistoryView => Mapper.MapHistoryView(dalHistoryView)).ToList(),
            };
            return stateView;
        }

        //************************************************************************************************ STATE
        /// <summary>
        /// Returns data about game progress, winnings and wallet entries for a logged in user.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>UserStateView object</returns>
        public virtual async Task<UserStateView?> GetUserStateViewAsync(Guid userId)
        {
            var dalUserWallets = await UOW.Wallets.GetUserWallets(userId);
            var bllUserWallets = dalUserWallets.Select(w => Mapper.MapUserWalletView(w)).ToList();
            var userRound = await UOW.GameRounds.GetLastActiveRound(userId);


            if (userRound == null) // if no active round, return generic user game data
            {
                userRound = await UOW.GameRounds.GetGenericUserGameData(userId);
                if (userRound == null)
                {
                    return null; // no state found for user.
                }

                return new UserStateView() // return generic game data for logged in user
                {
                    NickName = userRound.AppUser!.Nickname,
                    Status = userRound.RoundStatus!.Name,
                    Wallets = bllUserWallets,
                };
            }
            // else, continue with active round


            var wallet = await UOW.Wallets.GetWalletByCurrency(userRound.Configuration!.CurrencyTypeId, userId);
            if (wallet == null)
            {
                return null; // user has no active round or wallet
            }


            var userState = new UserStateView()
            {
                NickName = userRound.AppUser!.Nickname,
                Status = userRound.RoundStatus!.Name,
                Balance = wallet.Amount,
                CurrencyType = wallet.CurrencyType!.Name,
                ConfigurationId = userRound.ConfigurationId,
                CurrentLevel = userRound.CurrentLevel,
                Win = await UOW.GameHistories.GetRoundLastWin(userRound.Id),
                TotalWin = await UOW.GameHistories.GetRoundTotalWin(userRound.Id),
                Wallets = bllUserWallets,
            };
            return userState;
        }

        //************************************************************************************************ INIT
        /// <summary>
        /// Initialize new round with given configuration ID.
        /// ConfigurationId will determine what currency and UserWallet is used for the upcoming game.
        /// </summary>
        /// <param name="initRequest"></param>
        /// <param name="userId"></param>
        /// <returns>UserStateView</returns>
        public async Task<UserStateView?> InitRound(InitRequestView initRequest, Guid userId)
        {
            var activeSessionId = await UOW.GameRounds.GetLastActiveRound(userId);
            if (activeSessionId != null) // do not allow to create new round if old not resolved
            {
                return null;
            }

            var newRound = new BLL.App.DTO.GameRound()
            {
                AppUserId = userId,
                ConfigurationId = initRequest.ConfigurationId ?? default,
                CurrentLevel = 0,
                RoundStatusId = Enums.RoundStatusEnum.ActiveGuid // ACTIVE state for new round
            };

            var dalEntity = Mapper.Map(newRound);
            UOW.GameRounds.Add(dalEntity);
            await UOW.SaveChangesAsync();

            return await GetUserStateViewAsync(userId);
        }

        //************************************************************************************************ BET
        /// <summary>
        /// User gamble action, here user wagers amount to get a chance to win double back.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>UserStateView</returns>
        public async Task<UserStateView?> ExecuteBet(Guid userId)
        {
            // 1) need to check if any active round available
            // 2) if found, do gamble and update data
            var userRound = await UOW.GameRounds.GetLastActiveRound(userId);
            if (userRound == null)
            {
                return null; // no state found for user.
            }

            var success = await GambleAndResolveWallet(userId, userRound); // execute gamble
            if (success == false)
            {
                return null;
            }

            var userStateResult = await GetUserStateViewAsync(userId); // get state with updated winnings

            // Update round status & currentLevel in database, (do this here, otherwise GetUserState will not return RESOLVED round)
            await UOW.GameRounds.UpdateRoundAfterBet(userRound.Id, userRound.CurrentLevel, userRound.RoundStatusId);

            // return final round result
            userStateResult!.Status = userRound.RoundStatus!.Name;
            userStateResult.CurrentLevel = userRound.CurrentLevel;
            return userStateResult;
        }

        /// <summary>
        /// Do gamble and resolve all money related actions
        /// Update wallet with winnings or losses, create GameHistory entry,
        /// Resolve round if last level or user did not win gamble
        /// Execute simple RNG-(Random Number Generator) action with RTP(Return To Player) at about 80% & House Edge 20%
        /// </summary>
        /// <returns>Void</returns>
        private async Task<bool> GambleAndResolveWallet(Guid userId, GameRound userRound)
        {
            var nextLevelNr = userRound.CurrentLevel + 1;
            var wager = await UOW.Configurations.GetLevelBet(userRound.Configuration!.GameModeId, nextLevelNr);
            if (wager == null)
            {
                return false; // no bet value found for specified level gameMode
            }

            // validate bet, does user have enough money for bet action?
            var validBet = await ValidateBet(userRound, (double) wager);
            if (validBet == false)
            {
                return false; // not enough funds.
            }

            // update user winnings, increment level, add new game history entry
            var historyEntry = new DAL.App.DTO.GameHistory()
            {
                AppUserId = userId,
                Bet = wager,
                Win = 0.0,
                GameRoundId = userRound.Id
            };

            var isWin = new Random().NextDouble() >= 0.18; // about 82% RTP
            if (isWin)
            {
                historyEntry.Win = wager * 2; // return 2X wager for winnings
                await AddUserWinToWallet(userRound, historyEntry.Win ?? 0.0); // update wallet
            }

            UOW.GameHistories.Add(historyEntry); // update user winnings
            await UOW.SaveChangesAsync(); // save new gameHistory entry

            userRound.CurrentLevel = nextLevelNr; // update level even if user LOSE-es the round
            var levelsCount = await UOW.Configurations.GetLevelsCount(userRound.Configuration!.GameModeId);
            var isLastLevel = Math.Abs(levelsCount - userRound.CurrentLevel) < 0.1;

            if (isWin == false || isLastLevel) // RESOLVE round if last level, or LOSE
            {
                userRound.RoundStatusId = Enums.RoundStatusEnum.ResolvedGuid;
                userRound.RoundStatus!.Name = Enums.RoundStatusEnum.Resolved;
                if (isWin == false)
                {
                    await RemoveUserWagersFromWallet(userRound);
                }
            }

            return true; // success
        }

        /// <summary>
        /// Perform Bet validation, has user got enough money in wallet for the desired bet?
        /// </summary>
        /// <param name="userRound"></param>
        /// <param name="wager"></param>
        /// <returns>boolean</returns>
        private async Task<bool> ValidateBet(GameRound userRound, double wager)
        {
            var currencyTypeId = userRound.Configuration!.CurrencyTypeId;
            var userWallet = await UOW.Wallets.GetWalletByCurrency(currencyTypeId, userRound.AppUser!.Id);

            var isValid = userWallet!.Amount > wager;
            if (isValid)
            {
                await UOW.Wallets.ReduceWalletAmount(currencyTypeId, wager, userRound.AppUserId);
            }

            return isValid; // returns false if user has not enough money for bet
        }

        /// <summary>
        /// Increase users wallet with winnings.
        /// </summary>
        /// <param name="userRound"></param>
        /// <param name="amount"></param>
        /// <returns>Wallet object</returns>
        private async Task<Wallet> AddUserWinToWallet(GameRound userRound, double amount)
        {
            // var roundTotalWin = await UOW.GameHistories.GetRoundTotalWin(userRound.Id);
            var currencyId = userRound.Configuration!.CurrencyTypeId;
            var updatedWallet = await UOW.Wallets.IncreaseWalletAmount(currencyId, amount, userRound.AppUserId);

            return updatedWallet;
        }

        /// <summary>
        /// Reduce users wallet amount by wagers dealt in the round.
        /// </summary>
        /// <param name="userRound"></param>
        /// <returns>Wallet object</returns>
        private async Task<Wallet> RemoveUserWagersFromWallet(GameRound userRound)
        {
            // var roundTotalWin = await UOW.GameHistories.GetRoundTotalWin(userRound.Id);
            var currencyId = userRound.Configuration!.CurrencyTypeId;
            var totalRoundBet = await UOW.GameHistories.GetRoundTotalBet(userRound.Id);
            var updatedWallet = await UOW.Wallets.ReduceWalletAmount(currencyId, totalRoundBet, userRound.AppUserId);

            return updatedWallet;
        }

        //************************************************************************************************ RESOLVE 
        /// <summary>
        /// Attempt to RESOLVE an ACTIVE user round.
        /// This will block any BET actions done to the round.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>UserStateView object</returns>
        public async Task<UserStateView?> ResolveRound(Guid userId)
        {
            var activeUserRound = await UOW.GameRounds.GetLastActiveRound(userId);
            if (activeUserRound == null)
            {
                return null; // no state found for user.
            }

            var userStateView = await GetUserStateViewAsync(userId); // get user state for view
            userStateView!.Status = Enums.RoundStatusEnum.Resolved;

            // RESOLVE round status in database, (do this GetUserStateView, otherwise will not return RESOLVED round)
            await UOW.GameRounds.UpdateRoundAfterBet(activeUserRound.Id, activeUserRound.CurrentLevel,
                Enums.RoundStatusEnum.ResolvedGuid);

            // return final round result
            return userStateView;
        }
    }
}