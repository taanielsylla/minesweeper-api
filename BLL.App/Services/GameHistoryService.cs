using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.App.Mappers;
using ee.itcollege.tasull.BLL.@base.Services;
using Contracts.BLL.App.Mappers;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO.Views;

namespace BLL.App.Services
{
    public class GameHistoryService :
        BaseEntityService<IAppUnitOfWork, IGameHistoryRepository, IGameHistoryServiceMapper,
            DAL.App.DTO.GameHistory, BLL.App.DTO.GameHistory>, IGameHistoryService
    {
        public GameHistoryService(IAppUnitOfWork uow) : base(uow, uow.GameHistories,
            new GameHistoryServiceMapper())
        {
        }

        public Task<IEnumerable<HistoryView>> GetLastNEntries(int count)
        {
            throw new System.NotImplementedException();
        }
    }
}