using BLL.App.Mappers;
using ee.itcollege.tasull.BLL.@base.Services;
using Contracts.BLL.App.Mappers;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;

namespace BLL.App.Services
{
    public class FaucetService :
        BaseEntityService<IAppUnitOfWork, IFaucetRepository, IFaucetServiceMapper,
            DAL.App.DTO.Faucet, BLL.App.DTO.Faucet>, IFaucetService
    {
        public FaucetService(IAppUnitOfWork uow) : base(uow, uow.Faucets,
            new FaucetServiceMapper())
        {
        }
    }
}