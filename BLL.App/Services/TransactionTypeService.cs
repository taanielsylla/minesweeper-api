using BLL.App.Mappers;
using ee.itcollege.tasull.BLL.@base.Services;
using Contracts.BLL.App.Mappers;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;

namespace BLL.App.Services
{
    public class TransactionTypeService :
        BaseEntityService<IAppUnitOfWork, ITransactionTypeRepository, ITransactionTypeServiceMapper,
            DAL.App.DTO.TransactionType, BLL.App.DTO.TransactionType>, ITransactionTypeService
    {
        public TransactionTypeService(IAppUnitOfWork uow) : base(uow, uow.TransactionTypes,
            new TransactionTypeServiceMapper())
        {
        }
    }
}