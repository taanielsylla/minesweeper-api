﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ee.itcollege.tasull.Contracts.DAL.@base;
using ee.itcollege.tasull.Contracts.Domain;
using Domain.App;
using Domain.App.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class AppDbContext : IdentityDbContext<AppUser, AppRole, Guid>, IBaseEntityTracker
    {
        private readonly IUserNameProvider _userNameProvider;
        public DbSet<LangStr> LangStrs { get; set; } = default!;
        public DbSet<LangStrTranslation> LangStrTranslation { get; set; } = default!;


        public DbSet<Affiliate> Affiliates { get; set; } = default!;
        public DbSet<Configuration> Configurations { get; set; } = default!;
        public DbSet<CurrencyType> CurrencyTypes { get; set; } = default!;
        public DbSet<Faucet> Faucets { get; set; } = default!;
        public DbSet<GameHistory> GameHistories { get; set; } = default!;
        public DbSet<GameLevel> GameLevels { get; set; } = default!;
        public DbSet<GameMode> GameModes { get; set; } = default!;
        public DbSet<GameRound> GameRounds { get; set; } = default!;
        public DbSet<RoundStatus> RoundStatuses { get; set; } = default!;
        public DbSet<Transaction> Transactions { get; set; } = default!;
        public DbSet<TransactionStatus> TransactionStatuses { get; set; } = default!;
        public DbSet<TransactionType> TransactionTypes { get; set; } = default!;
        public DbSet<Wallet> Wallets { get; set; } = default!;


        private readonly Dictionary<IDomainEntityId<Guid>, IDomainEntityId<Guid>> _entityTracker =
            new Dictionary<IDomainEntityId<Guid>, IDomainEntityId<Guid>>();

        public AppDbContext(DbContextOptions<AppDbContext> options, IUserNameProvider userNameProvider) : base(options)
        {
            _userNameProvider = userNameProvider;
        }

        public void AddToEntityTracker(IDomainEntityId<Guid> internalEntity, IDomainEntityId<Guid> externalEntity)
        {
            _entityTracker.Add(internalEntity, externalEntity);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            // disable cascade delete
            foreach (var relationship in builder.Model
                .GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

            // enable cascade delete on LangStr->LangStrTranslations
            builder.Entity<LangStr>()
                .HasMany(s => s.Translations)
                .WithOne(l => l.LangStr!)
                .OnDelete(DeleteBehavior.Cascade);

            // enable cascade delete on GameRound->GameHistories
            builder.Entity<GameRound>()
                .HasMany(s => s.GameHistories)
                .WithOne(l => l.GameRound!)
                .OnDelete(DeleteBehavior.Cascade);

            // indexes
            builder.Entity<Affiliate>().HasIndex(i => i.CreatedAt);
            builder.Entity<Configuration>().HasIndex(i => i.CreatedAt);
            builder.Entity<CurrencyType>().HasIndex(i => i.CreatedAt);
            builder.Entity<Faucet>().HasIndex(i => i.CreatedAt);
            builder.Entity<GameHistory>().HasIndex(i => i.CreatedAt);
            builder.Entity<GameLevel>().HasIndex(i => i.CreatedAt);
            builder.Entity<GameMode>().HasIndex(i => i.CreatedAt);
            builder.Entity<GameRound>().HasIndex(i => i.CreatedAt);
            builder.Entity<RoundStatus>().HasIndex(i => i.CreatedAt);
            builder.Entity<Transaction>().HasIndex(i => i.CreatedAt);
            builder.Entity<TransactionStatus>().HasIndex(i => i.CreatedAt);
            builder.Entity<TransactionType>().HasIndex(i => i.CreatedAt);
            builder.Entity<Wallet>().HasIndex(i => i.CreatedAt);

            builder.Entity<LangStrTranslation>().HasIndex(i => new {i.Culture, i.LangStrId}).IsUnique();
        }

        private void SaveChangesMetadataUpdate()
        {
            // update the state of ef tracked objects
            ChangeTracker.DetectChanges();

            var markedAsAdded = ChangeTracker.Entries().Where(x => x.State == EntityState.Added);
            foreach (var entityEntry in markedAsAdded)
            {
                if (!(entityEntry.Entity is IDomainEntityMetadata entityWithMetaData)) continue;

                entityWithMetaData.CreatedAt = DateTime.Now;
                entityWithMetaData.CreatedBy = _userNameProvider.CurrentUserName;
                entityWithMetaData.ChangedAt = entityWithMetaData.CreatedAt;
                entityWithMetaData.ChangedBy = entityWithMetaData.CreatedBy;
            }

            var markedAsModified = ChangeTracker.Entries().Where(x => x.State == EntityState.Modified);
            foreach (var entityEntry in markedAsModified)
            {
                // check for IDomainEntityMetadata
                if (!(entityEntry.Entity is IDomainEntityMetadata entityWithMetaData)) continue;

                entityWithMetaData.ChangedAt = DateTime.Now;
                entityWithMetaData.ChangedBy = _userNameProvider.CurrentUserName;

                // do not let changes on these properties get into generated db sentences - db keeps old values
                entityEntry.Property(nameof(entityWithMetaData.CreatedAt)).IsModified = false;
                entityEntry.Property(nameof(entityWithMetaData.CreatedBy)).IsModified = false;
            }
        }

        private void UpdateTrackedEntities()
        {
            foreach (var (key, value) in _entityTracker)
            {
                value.Id = key.Id;
            }
        }

        public override int SaveChanges()
        {
            SaveChangesMetadataUpdate();
            var result = base.SaveChanges();
            UpdateTrackedEntities();
            return result;
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            SaveChangesMetadataUpdate();
            var result = base.SaveChangesAsync(cancellationToken);
            UpdateTrackedEntities();
            return result;
        }
    }
}