using System;
using System.Linq;
using Domain.App;
using Domain.App.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Helpers
{
    public class DataInitializers
    {
        public static void MigrateDatabase(AppDbContext context)
        {
            context.Database.Migrate();
        }

        public static void DeleteDatabase(AppDbContext context)
        {
            context.Database.EnsureDeleted();
        }


        public static void SeedData(AppDbContext context)
        {
            // insert predefined CurrencyTypes
            var currencyTypes = new[]
            {
                new CurrencyType() {Id = new Guid("00000000-0000-0000-0000-000000000001"), Name = "BTC",},
                new CurrencyType() {Id = new Guid("00000000-0000-0000-0000-000000000002"), Name = "mBTC",},
                new CurrencyType() {Id = new Guid("00000000-0000-0000-0000-000000000003"), Name = "Satoshi",},
            };
            foreach (var currencyType in currencyTypes)
            {
                if (!context.CurrencyTypes.Any(l => l.Id == currencyType.Id))
                {
                    context.CurrencyTypes.Add(currencyType);
                }
            }

            context.SaveChanges();

            // insert predefined GameModes
            var gameModes = new[]
            {
                new GameMode()
                    {Id = new Guid("00000000-0000-0000-0000-000000000001"), Name = "Easy", CountOfLevels = 4},
                new GameMode()
                    {Id = new Guid("00000000-0000-0000-0000-000000000002"), Name = "Medium", CountOfLevels = 7},
                new GameMode()
                    {Id = new Guid("00000000-0000-0000-0000-000000000003"), Name = "Hard", CountOfLevels = 10},
            };
            foreach (var gameMode in gameModes)
            {
                if (!context.GameModes.Any(l => l.Id == gameMode.Id))
                {
                    context.GameModes.Add(gameMode);
                }
            }

            context.SaveChanges();

            // insert predefined GameLevels
            var gameLevels = new[]
            {
                new GameLevel() // EASY DIFFICULTY
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000001"),
                    GameModeId = new Guid("00000000-0000-0000-0000-000000000001"),
                    Bet = 0.56, LevelNr = 1, BoxCount = 5
                },
                new GameLevel()
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000002"),
                    GameModeId = new Guid("00000000-0000-0000-0000-000000000001"),
                    Bet = 1.12, LevelNr = 2, BoxCount = 5
                },
                new GameLevel()
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000003"),
                    GameModeId = new Guid("00000000-0000-0000-0000-000000000001"),
                    Bet = 2.24, LevelNr = 3, BoxCount = 5
                },
                new GameLevel()
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000004"),
                    GameModeId = new Guid("00000000-0000-0000-0000-000000000001"),
                    Bet = 4.48, LevelNr = 4, BoxCount = 5
                },
                new GameLevel() // MEDIUM DIFFICULTY
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000005"),
                    GameModeId = new Guid("00000000-0000-0000-0000-000000000002"),
                    Bet = 1.44, LevelNr = 1, BoxCount = 4
                },
                new GameLevel()
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000006"),
                    GameModeId = new Guid("00000000-0000-0000-0000-000000000002"),
                    Bet = 2.88, LevelNr = 2, BoxCount = 4
                },
                new GameLevel()
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000007"),
                    GameModeId = new Guid("00000000-0000-0000-0000-000000000002"),
                    Bet = 5.76, LevelNr = 3, BoxCount = 4
                },
                new GameLevel()
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000008"),
                    GameModeId = new Guid("00000000-0000-0000-0000-000000000002"),
                    Bet = 11.52, LevelNr = 4, BoxCount = 4
                },
                new GameLevel()
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000009"),
                    GameModeId = new Guid("00000000-0000-0000-0000-000000000002"),
                    Bet = 23.04, LevelNr = 5, BoxCount = 4
                },
                new GameLevel()
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000010"),
                    GameModeId = new Guid("00000000-0000-0000-0000-000000000002"),
                    Bet = 46.08, LevelNr = 6, BoxCount = 4
                },
                new GameLevel()
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000011"),
                    GameModeId = new Guid("00000000-0000-0000-0000-000000000002"),
                    Bet = 92.16, LevelNr = 7, BoxCount = 4
                },
                new GameLevel() // HARD DIFFICULTY
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000012"),
                    GameModeId = new Guid("00000000-0000-0000-0000-000000000003"),
                    Bet = 3.22, LevelNr = 1, BoxCount = 3
                },
                new GameLevel()
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000013"),
                    GameModeId = new Guid("00000000-0000-0000-0000-000000000003"),
                    Bet = 6.44, LevelNr = 2, BoxCount = 3
                },
                new GameLevel()
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000014"),
                    GameModeId = new Guid("00000000-0000-0000-0000-000000000003"),
                    Bet = 12.88, LevelNr = 3, BoxCount = 3
                },
                new GameLevel()
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000015"),
                    GameModeId = new Guid("00000000-0000-0000-0000-000000000003"),
                    Bet = 25.76, LevelNr = 4, BoxCount = 3
                },
                new GameLevel()
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000016"),
                    GameModeId = new Guid("00000000-0000-0000-0000-000000000003"),
                    Bet = 51.52, LevelNr = 5, BoxCount = 3
                },
                new GameLevel()
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000017"),
                    GameModeId = new Guid("00000000-0000-0000-0000-000000000003"),
                    Bet = 103.04, LevelNr = 6, BoxCount = 3
                },
                new GameLevel()
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000018"),
                    GameModeId = new Guid("00000000-0000-0000-0000-000000000003"),
                    Bet = 206.08, LevelNr = 7, BoxCount = 3
                },
                new GameLevel()
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000019"),
                    GameModeId = new Guid("00000000-0000-0000-0000-000000000003"),
                    Bet = 412.16, LevelNr = 8, BoxCount = 3
                },
                new GameLevel()
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000020"),
                    GameModeId = new Guid("00000000-0000-0000-0000-000000000003"),
                    Bet = 824.32, LevelNr = 9, BoxCount = 3
                },
                new GameLevel()
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000021"),
                    GameModeId = new Guid("00000000-0000-0000-0000-000000000003"),
                    Bet = 1648.64, LevelNr = 10, BoxCount = 3
                },
            };
            foreach (var gameLevel in gameLevels)
            {
                if (!context.GameLevels.Any(l => l.Id == gameLevel.Id))
                {
                    context.GameLevels.Add(gameLevel);
                }
            }

            context.SaveChanges();

            // insert predefined Configurations
            var configurations = new[]
            {
                new Configuration() // BTC configurations
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000001"),
                    CurrencyTypeId = new Guid("00000000-0000-0000-0000-000000000001"),
                    Name = "BTC - Easy",
                    GameModeId = new Guid("00000000-0000-0000-0000-000000000001"),
                },
                new Configuration()
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000002"),
                    CurrencyTypeId = new Guid("00000000-0000-0000-0000-000000000001"),
                    Name = "BTC - Medium",
                    GameModeId = new Guid("00000000-0000-0000-0000-000000000002"),
                },
                new Configuration()
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000003"),
                    CurrencyTypeId = new Guid("00000000-0000-0000-0000-000000000001"),
                    Name = "BTC - Hard",
                    GameModeId = new Guid("00000000-0000-0000-0000-000000000003"),
                },
                new Configuration() // mBTC configurations
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000004"),
                    CurrencyTypeId = new Guid("00000000-0000-0000-0000-000000000002"),
                    Name = "mBTC - Easy",
                    GameModeId = new Guid("00000000-0000-0000-0000-000000000001"),
                },
                new Configuration()
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000005"),
                    CurrencyTypeId = new Guid("00000000-0000-0000-0000-000000000002"),
                    Name = "mBTC - Medium",
                    GameModeId = new Guid("00000000-0000-0000-0000-000000000002"),
                },
                new Configuration()
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000006"),
                    CurrencyTypeId = new Guid("00000000-0000-0000-0000-000000000002"),
                    Name = "mBTC - Hard",
                    GameModeId = new Guid("00000000-0000-0000-0000-000000000003"),
                },
                new Configuration() // Satoshi configurations
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000007"),
                    CurrencyTypeId = new Guid("00000000-0000-0000-0000-000000000003"),
                    Name = "Satoshi - Easy",
                    GameModeId = new Guid("00000000-0000-0000-0000-000000000001"),
                },
                new Configuration()
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000008"),
                    CurrencyTypeId = new Guid("00000000-0000-0000-0000-000000000003"),
                    Name = "Satoshi - Medium",
                    GameModeId = new Guid("00000000-0000-0000-0000-000000000002"),
                },
                new Configuration()
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000009"),
                    CurrencyTypeId = new Guid("00000000-0000-0000-0000-000000000003"),
                    Name = "Satoshi - Hard",
                    GameModeId = new Guid("00000000-0000-0000-0000-000000000003"),
                },
            };
            foreach (var conf in configurations)
            {
                if (!context.Configurations.Any(l => l.Id == conf.Id))
                {
                    context.Configurations.Add(conf);
                }
            }

            context.SaveChanges();

            // insert predefined RoundStatuses
            var roundStatuses = new[]
            {
                new RoundStatus() {Id = new Guid("00000000-0000-0000-0000-000000000001"), Name = "NO_GAME",},
                new RoundStatus() {Id = new Guid("00000000-0000-0000-0000-000000000002"), Name = "ACTIVE",},
                new RoundStatus() {Id = new Guid("00000000-0000-0000-0000-000000000003"), Name = "RESOLVED",},
            };
            foreach (var roundStatus in roundStatuses)
            {
                if (!context.RoundStatuses.Any(l => l.Id == roundStatus.Id))
                {
                    context.RoundStatuses.Add(roundStatus);
                }
            }

            // insert predefined TransactionTypes
            var transactionTypes = new[]
            {
                new TransactionType() {Id = new Guid("00000000-0000-0000-0000-000000000001"), Name = "DEPOSIT"},
                new TransactionType() {Id = new Guid("00000000-0000-0000-0000-000000000002"), Name = "WITHDRAW"},
                new TransactionType() {Id = new Guid("00000000-0000-0000-0000-000000000003"), Name = "BET"},
                new TransactionType() {Id = new Guid("00000000-0000-0000-0000-000000000004"), Name = "WIN"},
            };
            foreach (var transactionType in transactionTypes)
            {
                if (!context.TransactionTypes.Any(l => l.Id == transactionType.Id))
                {
                    context.TransactionTypes.Add(transactionType);
                }
            }

            // insert predefined TransactionStatuses
            var transactionStatuses = new[]
            {
                new TransactionStatus() {Id = new Guid("00000000-0000-0000-0000-000000000001"), Name = "PENDING",},
                new TransactionStatus() {Id = new Guid("00000000-0000-0000-0000-000000000002"), Name = "COMPLETED",},
                new TransactionStatus() {Id = new Guid("00000000-0000-0000-0000-000000000003"), Name = "FAILED",},
            };
            foreach (var transactionStatus in transactionStatuses)
            {
                if (!context.TransactionStatuses.Any(l => l.Id == transactionStatus.Id))
                {
                    context.TransactionStatuses.Add(transactionStatus);
                }
            }

            // insert predefined Wallets
            var wallets = new[] // every user should have 3 wallets on registration (BTC, mBTC & Satoshi)
            {
                // For user test@test.com --------------------------------------------

                new Wallet() // BTC wallet
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000001"),
                    AppUserId = new Guid("00000000-0000-0000-0000-000000000001"),
                    CurrencyTypeId = new Guid("00000000-0000-0000-0000-000000000001"),
                    Amount = 947.80,
                },
                new Wallet() // mBTC wallet
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000002"),
                    AppUserId = new Guid("00000000-0000-0000-0000-000000000001"),
                    CurrencyTypeId = new Guid("00000000-0000-0000-0000-000000000002"),
                    Amount = 769.35,
                },
                new Wallet() // Satoshi wallet
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000003"),
                    AppUserId = new Guid("00000000-0000-0000-0000-000000000001"),
                    CurrencyTypeId = new Guid("00000000-0000-0000-0000-000000000003"),
                    Amount = 833.19,
                },

                // For user test2@test.com --------------------------------------------

                new Wallet() // BTC wallet
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000004"),
                    AppUserId = new Guid("00000000-0000-0000-0000-000000000002"),
                    CurrencyTypeId = new Guid("00000000-0000-0000-0000-000000000001"),
                    Amount = 947.80,
                },
                new Wallet() // mBTC wallet
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000005"),
                    AppUserId = new Guid("00000000-0000-0000-0000-000000000002"),
                    CurrencyTypeId = new Guid("00000000-0000-0000-0000-000000000002"),
                    Amount = 769.35,
                },
                new Wallet() // Satoshi wallet
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000006"),
                    AppUserId = new Guid("00000000-0000-0000-0000-000000000002"),
                    CurrencyTypeId = new Guid("00000000-0000-0000-0000-000000000003"),
                    Amount = 833.19,
                },
            };
            foreach (var wallet in wallets)
            {
                if (!context.Wallets.Any(l => l.Id == wallet.Id))
                {
                    context.Wallets.Add(wallet);
                }
            }

            context.SaveChanges();

            // insert predefined Rounds for user
            var rounds = new[] // insert one of each type, ACTIVE, NO_GAME & RESOLVED
            {
                new GameRound() // ACTIVE
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000001"),
                    AppUserId = new Guid("00000000-0000-0000-0000-000000000001"),
                    RoundStatusId = new Guid("00000000-0000-0000-0000-000000000002"),
                    ConfigurationId = new Guid("00000000-0000-0000-0000-000000000001"),
                    CurrentLevel = 3
                },
                new
                    GameRound() // NO_GAME - should not exist in the field. When user presses "START" btn, an ACTIVE round is created directly
                    {
                        Id = new Guid("00000000-0000-0000-0000-000000000002"),
                        AppUserId = new Guid("00000000-0000-0000-0000-000000000001"),
                        RoundStatusId = new Guid("00000000-0000-0000-0000-000000000001"),
                        ConfigurationId = new Guid("00000000-0000-0000-0000-000000000003"),
                        CurrentLevel = 0
                    },
                new GameRound() // RESOLVED
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000003"),
                    AppUserId = new Guid("00000000-0000-0000-0000-000000000001"),
                    RoundStatusId = new Guid("00000000-0000-0000-0000-000000000003"),
                    ConfigurationId = new Guid("00000000-0000-0000-0000-000000000002"),
                    CurrentLevel = 7
                },
            };
            foreach (var round in rounds)
            {
                if (!context.GameRounds.Any(r => r.Id == round.Id))
                {
                    context.GameRounds.Add(round);
                }
            }

            context.SaveChanges();

            // insert game histories, mock player betting
            var histories = new[]
            {
                new GameHistory() // for RESOLVE round
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000001"),
                    AppUserId = new Guid("00000000-0000-0000-0000-000000000001"),
                    GameRoundId = new Guid("00000000-0000-0000-0000-000000000002"),
                    Bet = 0.45,
                    Win = 0.45
                },
                new GameHistory() // for RESOLVE round
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000002"),
                    AppUserId = new Guid("00000000-0000-0000-0000-000000000001"),
                    GameRoundId = new Guid("00000000-0000-0000-0000-000000000002"),
                    Bet = 1.9,
                    Win = 1.9
                },
                new GameHistory() // for RESOLVE round
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000003"),
                    AppUserId = new Guid("00000000-0000-0000-0000-000000000001"),
                    GameRoundId = new Guid("00000000-0000-0000-0000-000000000002"),
                    Bet = 4.8,
                    Win = 4.8
                },
                new GameHistory() // for RESOLVE round
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000004"),
                    AppUserId = new Guid("00000000-0000-0000-0000-000000000001"),
                    GameRoundId = new Guid("00000000-0000-0000-0000-000000000002"),
                    Bet = 13.2,
                    Win = 13.2
                },
                new GameHistory() // for RESOLVE round
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000005"),
                    AppUserId = new Guid("00000000-0000-0000-0000-000000000001"),
                    GameRoundId = new Guid("00000000-0000-0000-0000-000000000002"),
                    Bet = 38.7,
                    Win = 38.7
                },
                new GameHistory() // for RESOLVE round
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000006"),
                    AppUserId = new Guid("00000000-0000-0000-0000-000000000001"),
                    GameRoundId = new Guid("00000000-0000-0000-0000-000000000002"),
                    Bet = 117.1,
                    Win = 117.1
                },
                new GameHistory() // for RESOLVE round
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000007"),
                    AppUserId = new Guid("00000000-0000-0000-0000-000000000001"),
                    GameRoundId = new Guid("00000000-0000-0000-0000-000000000002"),
                    Bet = 270.8,
                    Win = 270.8
                },
                new GameHistory() // for ACTIVE round
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000008"),
                    AppUserId = new Guid("00000000-0000-0000-0000-000000000001"),
                    GameRoundId = new Guid("00000000-0000-0000-0000-000000000001"),
                    Bet = 1.5,
                    Win = 1.5
                },
                new GameHistory() // for ACTIVE round
                {
                    Id = new Guid("00000000-0000-0000-0000-000000000009"),
                    AppUserId = new Guid("00000000-0000-0000-0000-000000000001"),
                    GameRoundId = new Guid("00000000-0000-0000-0000-000000000001"),
                    Bet = 4.5,
                    Win = 4.5
                },
            };
            foreach (var history in histories)
            {
                if (!context.GameHistories.Any(r => r.Id == history.Id))
                {
                    context.GameHistories.Add(history);
                }
            }

            context.SaveChanges();
        }

        public static void SeedIdentity(UserManager<AppUser> userManager, RoleManager<AppRole> roleManager)
        {
            var roles = new (string roleName, string roleDisplayName)[]
            {
                ("user", "User"),
                ("admin", "Admin")
            };

            foreach (var (roleName, roleDisplayName) in roles)
            {
                var role = roleManager.FindByNameAsync(roleName).Result;
                if (role == null)
                {
                    role = new AppRole()
                    {
                        Name = roleName,
                        DisplayName = roleDisplayName
                    };

                    var result = roleManager.CreateAsync(role).Result;
                    if (!result.Succeeded)
                    {
                        throw new ApplicationException("Role creation failed!");
                    }
                }
            }


            var users =
                new (string name, string password, string firstName, string lastName, Guid Id, string[] roles, string
                    nickname)[]
                    {
                        ("test@test.com", "Kala.maja.2020", "Nipi", "Tiri",
                            new Guid("00000000-0000-0000-0000-000000000001"), new[] {"admin", "user"}, "EpicWinner123"),
                        ("test2@test.com", "Kala.maja.2020", "Tamme", "T6ru",
                            new Guid("00000000-0000-0000-0000-000000000002"), new[] {"user"}, "MasterBlaster123"),
                    };

            foreach (var userInfo in users)
            {
                var user = userManager.FindByEmailAsync(userInfo.name).Result;
                if (user == null)
                {
                    user = new AppUser()
                    {
                        Id = userInfo.Id,
                        Email = userInfo.name,
                        UserName = userInfo.name,
                        FirstName = userInfo.firstName,
                        LastName = userInfo.lastName,
                        EmailConfirmed = true,
                        Nickname = userInfo.nickname
                    };
                    var result = userManager.CreateAsync(user, userInfo.password).Result;
                    if (!result.Succeeded)
                    {
                        throw new ApplicationException("User creation failed!");
                    }
                }

                foreach (var role in userInfo.roles)
                {
                    var roleResult = userManager.AddToRoleAsync(user, role).Result;
                }
            }
        }
    }
}