using System.Collections.Generic;
using AutoMapper;
using DAL.Base.Mappers;

// using ee.itcollege.tasull.DAL.@base.Mappers;

namespace DAL.App.EF.Mappers
{
    public class DALMapper<TLeftObject, TRightObject> : BaseMapper<TLeftObject, TRightObject>
        where TRightObject : class?, new()
        where TLeftObject : class?, new()
    {
        public DALMapper() : base()
        {
            // add more mappings
            MapperConfigurationExpression.CreateMap<Domain.App.Identity.AppUser, DAL.App.DTO.Identity.AppUser>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.Identity.AppUser, Domain.App.Identity.AppUser>();


            MapperConfigurationExpression.CreateMap<Domain.App.Affiliate, DAL.App.DTO.Affiliate>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.Affiliate, Domain.App.Affiliate>();

            MapperConfigurationExpression.CreateMap<Domain.App.Configuration, DAL.App.DTO.Configuration>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.Configuration, Domain.App.Configuration>();

            MapperConfigurationExpression.CreateMap<Domain.App.CurrencyType, DAL.App.DTO.CurrencyType>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.CurrencyType, Domain.App.CurrencyType>();

            MapperConfigurationExpression.CreateMap<Domain.App.Faucet, DAL.App.DTO.Faucet>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.Faucet, Domain.App.Faucet>();

            MapperConfigurationExpression.CreateMap<Domain.App.GameHistory, DAL.App.DTO.GameHistory>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.GameHistory, Domain.App.GameHistory>();

            MapperConfigurationExpression.CreateMap<Domain.App.GameLevel, DAL.App.DTO.GameLevel>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.GameLevel, Domain.App.GameLevel>();

            MapperConfigurationExpression.CreateMap<Domain.App.GameMode, DAL.App.DTO.GameMode>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.GameMode, Domain.App.GameMode>();

            MapperConfigurationExpression.CreateMap<Domain.App.GameRound, DAL.App.DTO.GameRound>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.GameRound, Domain.App.GameRound>();

            MapperConfigurationExpression.CreateMap<Domain.App.RoundStatus, DAL.App.DTO.RoundStatus>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.RoundStatus, Domain.App.RoundStatus>();

            MapperConfigurationExpression.CreateMap<Domain.App.Transaction, DAL.App.DTO.Transaction>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.Transaction, Domain.App.Transaction>();

            MapperConfigurationExpression.CreateMap<Domain.App.TransactionStatus, DAL.App.DTO.TransactionStatus>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.TransactionStatus, Domain.App.TransactionStatus>();

            MapperConfigurationExpression.CreateMap<Domain.App.TransactionType, DAL.App.DTO.TransactionType>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.TransactionType, Domain.App.TransactionType>();

            MapperConfigurationExpression.CreateMap<Domain.App.Wallet, DAL.App.DTO.Wallet>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.Wallet, Domain.App.Wallet>();

            Mapper = new Mapper(new MapperConfiguration(MapperConfigurationExpression));
        }
    }
}