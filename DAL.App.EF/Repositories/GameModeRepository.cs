using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using ee.itcollege.tasull.DAL.@base.EF.Repositories;

namespace DAL.App.EF.Repositories
{
    public class GameModeRepository :
        EFBaseRepository<AppDbContext, Domain.App.Identity.AppUser, Domain.App.GameMode, DAL.App.DTO.GameMode>,
        IGameModeRepository
    {
        public GameModeRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALMapper<Domain.App.GameMode, DTO.GameMode>())
        {
        }
    }
}