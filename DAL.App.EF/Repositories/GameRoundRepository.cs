using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO;
using DAL.App.DTO.Identity;
using DAL.App.EF.Mappers;
using ee.itcollege.tasull.DAL.@base.EF.Repositories;
using Enums;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class GameRoundRepository :
        EFBaseRepository<AppDbContext, Domain.App.Identity.AppUser, Domain.App.GameRound, DAL.App.DTO.GameRound>,
        IGameRoundRepository
    {
        public GameRoundRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALMapper<Domain.App.GameRound, DTO.GameRound>())
        {
        }

        // Here we return active round data for logged in user, round status, active configuration etc.
        public async Task<GameRound?> GetLastActiveRound(Guid userId)
        {
            var query = PrepareQuery(userId, true);
            query = query
                .Include(g => g.AppUser)
                .ThenInclude(g => g!.Wallets)
                .Include(g => g.Configuration)
                .Include(g => g.RoundStatus)
                .Where(round => round.RoundStatus!.Name == RoundStatusEnum.Active)
                .OrderBy(r => r.CreatedAt);

            var domainItem = await query.FirstOrDefaultAsync();
            if (domainItem != null)
            {
                return Mapper.Map(domainItem);
            }

            return null;
        }

        // if a round is not started for user, then here we return general information about user game data, username, status etc.
        public async Task<GameRound?> GetGenericUserGameData(Guid userId)
        {
            var appUser = await RepoDbContext.Users
                .Where(u => u.Id == userId)
                .Select(u => new AppUser() {Nickname = u.Nickname})
                .FirstOrDefaultAsync();

            return new DAL.App.DTO.GameRound()
            {
                AppUser = appUser,
                RoundStatus = new RoundStatus() {Name = RoundStatusEnum.NoGame},
            };
        }

        public async Task UpdateRoundAfterBet(Guid id, int newLevel, Guid newStatus)
        {
            var entry = await RepoDbSet.FindAsync(id);
            if (entry != null)
            {
                entry.CurrentLevel = newLevel;
                entry.RoundStatusId = newStatus;
                await RepoDbContext.SaveChangesAsync();
            }
        }
    }
}