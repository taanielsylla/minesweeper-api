using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using ee.itcollege.tasull.DAL.@base.EF.Repositories;

namespace DAL.App.EF.Repositories
{
    public class TransactionRepository :
        EFBaseRepository<AppDbContext, Domain.App.Identity.AppUser, Domain.App.Transaction, DAL.App.DTO.Transaction>,
        ITransactionRepository
    {
        public TransactionRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALMapper<Domain.App.Transaction, DTO.Transaction>())
        {
        }
    }
}