using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using ee.itcollege.tasull.DAL.@base.EF.Repositories;

namespace DAL.App.EF.Repositories
{
    public class RoundStatusRepository :
        EFBaseRepository<AppDbContext, Domain.App.Identity.AppUser, Domain.App.RoundStatus, DAL.App.DTO.RoundStatus>,
        IRoundStatusRepository
    {
        public RoundStatusRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALMapper<Domain.App.RoundStatus, DTO.RoundStatus>())
        {
        }
    }
}