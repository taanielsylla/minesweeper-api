using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO;
using DAL.App.DTO.Views;
using DAL.App.EF.Mappers;
using ee.itcollege.tasull.DAL.@base.EF.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class WalletRepository :
        EFBaseRepository<AppDbContext, Domain.App.Identity.AppUser, Domain.App.Wallet, DAL.App.DTO.Wallet>,
        IWalletRepository
    {
        public WalletRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALMapper<Domain.App.Wallet, DTO.Wallet>())
        {
        }

        public async Task<Wallet?> GetWalletByCurrency(Guid currencyId, Guid userId)
        {
            var wallet = await RepoDbSet.AsNoTracking()
                .Include(w => w.CurrencyType)
                .Where(w => w.CurrencyTypeId == currencyId && w.AppUserId == userId)
                .FirstOrDefaultAsync();

            return Mapper.Map(wallet);
        }

        public async Task<Wallet> ReduceWalletAmount(Guid currencyId, double amount, Guid userId)
        {
            var wallet = await RepoDbSet
                .Where(w => w.CurrencyTypeId == currencyId && w.AppUserId == userId)
                .FirstOrDefaultAsync();

            wallet.Amount -= amount;
            await RepoDbContext.SaveChangesAsync();

            return Mapper.Map(wallet);
        }

        public async Task<Wallet> IncreaseWalletAmount(Guid currencyId, double amount, Guid userId)
        {
            var wallet = await RepoDbSet
                .Where(w => w.CurrencyTypeId == currencyId && w.AppUserId == userId)
                .FirstOrDefaultAsync();

            wallet.Amount += amount;
            await RepoDbContext.SaveChangesAsync();

            return Mapper.Map(wallet);
        }

        public async Task<List<UserWalletView>> GetUserWallets(Guid userId)
        {
            var wallets = await RepoDbSet
                .Include(w => w.CurrencyType)
                .Where(w => w.AppUserId == userId)
                .Select(domainWallet => new UserWalletView()
                {
                    CurrencyName = domainWallet.CurrencyType!.Name,
                    Amount = domainWallet.Amount,
                })
                .ToListAsync();

            return wallets;
        }
    }
}