using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO;
using DAL.App.DTO.Views;
using DAL.App.EF.Mappers;
using ee.itcollege.tasull.DAL.@base.EF.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class GameHistoryRepository :
        EFBaseRepository<AppDbContext, Domain.App.Identity.AppUser, Domain.App.GameHistory, DAL.App.DTO.GameHistory>,
        IGameHistoryRepository
    {
        public GameHistoryRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALMapper<Domain.App.GameHistory, DTO.GameHistory>())
        {
        }

        public async Task<IEnumerable<HistoryView>> GetLastNEntries(int n = 10) // default 10 last entries
        {
            var entries = await RepoDbSet
                .Include(h => h.AppUser)
                .OrderByDescending(h => h.CreatedAt)
                .Take(n)
                .Select(h => new HistoryView()
                {
                    NickName = h.AppUser!.Nickname,
                    Bet = h.Bet,
                    Win = h.Win,
                })
                .ToListAsync();

            return entries;
        }

        public async Task<IEnumerable<GameHistory>> GetRoundHistoryAsync(Guid roundId)
        {
            var entries = await RepoDbSet
                .AsNoTracking()
                .Where(history => history.GameRoundId == roundId)
                .OrderByDescending(r => r.Bet!.Value)
                .ToListAsync();

            return entries.Select(e => Mapper.Map(e));
        }

        public async Task<double> GetRoundTotalWin(Guid roundId)
        {
            var histories = await GetRoundHistoryAsync(roundId);
            var totalWin = 0.0;

            // if last winning is 0, this means user lost the round.
            var isAnyZero = histories.Any(h => h.Win < 0.01); // take into account floating point error

            if (histories.Any() && isAnyZero == false)
            {
                foreach (var history in histories)
                {
                    totalWin += history.Win ?? 0.0;
                }
            }

            return totalWin; // result is zero, if last win was 0 (last wager lost everything)
        }

        public async Task<double> GetRoundLastWin(Guid roundId)
        {
            var lastWin = await RepoDbSet
                .AsNoTracking()
                .Where(history => history.GameRoundId == roundId)
                .OrderByDescending(r => r.CreatedAt)
                .Select(h => h.Win)
                .FirstOrDefaultAsync();

            return lastWin ?? 0.0;
        }

        public async Task<double> GetRoundTotalBet(Guid roundId)
        {
            var bets = await RepoDbSet
                .AsNoTracking()
                .Where(history => history.GameRoundId == roundId)
                .Select(h => h.Bet)
                .ToListAsync();

            return bets.Sum() ?? 0.0;
        }
    }
}