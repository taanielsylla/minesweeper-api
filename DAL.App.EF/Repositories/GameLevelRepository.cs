using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using ee.itcollege.tasull.DAL.@base.EF.Repositories;

namespace DAL.App.EF.Repositories
{
    public class GameLevelRepository :
        EFBaseRepository<AppDbContext, Domain.App.Identity.AppUser, Domain.App.GameLevel, DAL.App.DTO.GameLevel>,
        IGameLevelRepository
    {
        public GameLevelRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALMapper<Domain.App.GameLevel, DTO.GameLevel>())
        {
        }
    }
}