using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO.Views;
using DAL.App.EF.Mappers;
using ee.itcollege.tasull.DAL.@base.EF.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class ConfigurationRepository :
        EFBaseRepository<AppDbContext, Domain.App.Identity.AppUser, Domain.App.Configuration, DAL.App.DTO.Configuration
        >,
        IConfigurationRepository
    {
        public ConfigurationRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALMapper<Domain.App.Configuration, DTO.Configuration>())
        {
        }

        public async Task<IEnumerable<ConfigurationView>> GetAllForViewAsync()
        {
            var configurations = await RepoDbSet
                .Include(config => config.CurrencyType)
                .Include(config => config.GameMode)
                .ToListAsync();

            var levels = await RepoDbContext.GameLevels
                .ToListAsync();


            List<ConfigurationView> result = new List<ConfigurationView>();
            foreach (var config in configurations)
            {
                var bets = levels.FindAll(l => l.GameModeId == config.GameModeId)
                    .Select(i => i.Bet.ToString()).ToList();

                var boxCount = levels.Find(l => l.GameModeId == config.GameModeId).BoxCount;

                result.Add(new ConfigurationView()
                    {
                        Id = config.Id,
                        Name = config.Name,
                        GameMode = config.GameMode!.Name,
                        BoxCount = boxCount,
                        Bets = bets,
                        LevelCount = bets.Count,
                        CurrencyType = config.CurrencyType!.Name,
                    }
                );
            }

            return result;
        }

        public async Task<double?> GetLevelBet(Guid gameModeId, int levelNr)
        {
            var gameLevel = await RepoDbContext.GameLevels
                .AsNoTracking()
                .Include(l => l.GameMode)
                .Where(l => l.GameMode!.Id == gameModeId && l.LevelNr == levelNr)
                .FirstOrDefaultAsync();

            return gameLevel?.Bet;
        }

        public async Task<double> GetLevelsCount(Guid gameModeId)
        {
            var levels = await RepoDbContext.GameLevels
                .AsNoTracking()
                .Include(l => l.GameMode)
                .Where(l => l.GameMode!.Id == gameModeId)
                .ToListAsync();

            return levels.Count;
        }
    }
}