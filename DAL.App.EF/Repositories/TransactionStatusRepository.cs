using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using ee.itcollege.tasull.DAL.@base.EF.Repositories;

namespace DAL.App.EF.Repositories
{
    public class TransactionStatusRepository :
        EFBaseRepository<AppDbContext, Domain.App.Identity.AppUser, Domain.App.TransactionStatus,
            DAL.App.DTO.TransactionStatus>,
        ITransactionStatusRepository
    {
        public TransactionStatusRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALMapper<Domain.App.TransactionStatus, DTO.TransactionStatus>())
        {
        }
    }
}