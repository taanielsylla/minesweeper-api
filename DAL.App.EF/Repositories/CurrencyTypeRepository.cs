using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using ee.itcollege.tasull.DAL.@base.EF.Repositories;

namespace DAL.App.EF.Repositories
{
    public class CurrencyTypeRepository :
        EFBaseRepository<AppDbContext, Domain.App.Identity.AppUser, Domain.App.CurrencyType, DAL.App.DTO.CurrencyType>,
        ICurrencyTypeRepository
    {
        public CurrencyTypeRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALMapper<Domain.App.CurrencyType, DTO.CurrencyType>())
        {
        }
    }
}