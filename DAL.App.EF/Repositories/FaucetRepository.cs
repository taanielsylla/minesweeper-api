using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using ee.itcollege.tasull.DAL.@base.EF.Repositories;

namespace DAL.App.EF.Repositories
{
    public class FaucetRepository :
        EFBaseRepository<AppDbContext, Domain.App.Identity.AppUser, Domain.App.Faucet, DAL.App.DTO.Faucet>,
        IFaucetRepository
    {
        public FaucetRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALMapper<Domain.App.Faucet, DTO.Faucet>())
        {
        }
    }
}