using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using ee.itcollege.tasull.DAL.@base.EF.Repositories;

namespace DAL.App.EF.Repositories
{
    public class AffiliateRepository :
        EFBaseRepository<AppDbContext, Domain.App.Identity.AppUser, Domain.App.Affiliate, DAL.App.DTO.Affiliate>,
        IAffiliateRepository
    {
        public AffiliateRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALMapper<Domain.App.Affiliate, DTO.Affiliate>())
        {
        }
    }
}