using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using ee.itcollege.tasull.DAL.@base.EF.Repositories;

namespace DAL.App.EF.Repositories
{
    public class TransactionTypeRepository :
        EFBaseRepository<AppDbContext, Domain.App.Identity.AppUser, Domain.App.TransactionType,
            DAL.App.DTO.TransactionType>,
        ITransactionTypeRepository
    {
        public TransactionTypeRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALMapper<Domain.App.TransactionType, DTO.TransactionType>())
        {
        }
    }
}