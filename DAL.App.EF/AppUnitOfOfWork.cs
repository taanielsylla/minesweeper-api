using System;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using DAL.App.EF.Repositories;
using ee.itcollege.tasull.DAL.@base.EF;

namespace DAL.App.EF
{
    public class AppUnitOfOfWork : EFBaseUnitOfWork<Guid, AppDbContext>, IAppUnitOfWork
    {
        public AppUnitOfOfWork(AppDbContext uowDbContext) : base(uowDbContext)
        {
        }

        public ILangStrRepository LangStrs =>
            GetRepository<ILangStrRepository>(() => new LangStrRepository(UOWDbContext));

        public ILangStrTranslationRepository LangStrTranslations =>
            GetRepository<ILangStrTranslationRepository>(() => new LangStrTranslationRepository(UOWDbContext));

        public IAffiliateRepository Affiliates =>
            GetRepository<IAffiliateRepository>(() => new AffiliateRepository(UOWDbContext));

        public IConfigurationRepository Configurations =>
            GetRepository<IConfigurationRepository>(() => new ConfigurationRepository(UOWDbContext));

        public ICurrencyTypeRepository CurrencyTypes =>
            GetRepository<ICurrencyTypeRepository>(() => new CurrencyTypeRepository(UOWDbContext));

        public IFaucetRepository Faucets =>
            GetRepository<IFaucetRepository>(() => new FaucetRepository(UOWDbContext));

        public IGameHistoryRepository GameHistories =>
            GetRepository<IGameHistoryRepository>(() => new GameHistoryRepository(UOWDbContext));

        public IGameLevelRepository GameLevels =>
            GetRepository<IGameLevelRepository>(() => new GameLevelRepository(UOWDbContext));

        public IGameModeRepository GameModes =>
            GetRepository<IGameModeRepository>(() => new GameModeRepository(UOWDbContext));

        public IGameRoundRepository GameRounds =>
            GetRepository<IGameRoundRepository>(() => new GameRoundRepository(UOWDbContext));

        public IRoundStatusRepository RoundStatuses =>
            GetRepository<IRoundStatusRepository>(() => new RoundStatusRepository(UOWDbContext));

        public ITransactionRepository Transactions =>
            GetRepository<ITransactionRepository>(() => new TransactionRepository(UOWDbContext));

        public ITransactionStatusRepository TransactionStatuses =>
            GetRepository<ITransactionStatusRepository>(() => new TransactionStatusRepository(UOWDbContext));

        public ITransactionTypeRepository TransactionTypes =>
            GetRepository<ITransactionTypeRepository>(() => new TransactionTypeRepository(UOWDbContext));

        public IWalletRepository Wallets =>
            GetRepository<IWalletRepository>(() => new WalletRepository(UOWDbContext));
    }
}