﻿using System.Collections.Generic;

namespace BLL.App.DTO.Views
{
    public class StateView
    {
        public IList<string>? GameModes { get; set; }
        
        public IList<string>? Currencies { get; set; }

        public IList<ConfigurationView>?
            Configurations { get; set; } // all available configurations to start a game with

        public IList<HistoryView>? GameHistory { get; set; } // Some last winnings that players have had
    }
}