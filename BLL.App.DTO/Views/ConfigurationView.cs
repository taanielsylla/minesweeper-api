﻿using System;
using System.Collections.Generic;

namespace BLL.App.DTO.Views
{
    public class ConfigurationView
    {
        public Guid? Id { get; set; } // id of the configuration, used for INIT action

        public string? Name { get; set; } // e.g Hard - BTC

        public string? GameMode { get; set; } // Hard, Medium; Easy

        public int? BoxCount { get; set; } // Count of boxes for one level

        public IList<string>? Bets { get; set; } // bet amount for each level, these are win amounts also

        public int? LevelCount { get; set; } // Count of levels in a game

        public string? CurrencyType { get; set; } // The currency this configuration allows betting
    }
}