﻿using System;
using BLL.App.DTO.Identity;
using ee.itcollege.tasull.Contracts.Domain;

namespace BLL.App.DTO
{
    public class GameHistory : IDomainEntityId
    {
        public Guid Id { get; set; }

        public Guid AppUserId { get; set; }
        public AppUser? AppUser { get; set; }
        
        public Guid GameRoundId { get; set; }
        public GameRound? GameRound { get; set; }

        public double? Bet { get; set; }
        public double? Win { get; set; }
    }
}