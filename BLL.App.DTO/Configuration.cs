﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ee.itcollege.tasull.Contracts.Domain;

namespace BLL.App.DTO
{
    public class Configuration : IDomainEntityId
    {
        public Guid Id { get; set; }

        public Guid CurrencyTypeId { get; set; }
        public CurrencyType? CurrencyType { get; set; }

        [MaxLength(128)] [MinLength(1)] public string Name { get; set; } = default!;

        public ICollection<GameRound>? GameRounds { get; set; }

        public Guid GameModeId { get; set; }
        public GameMode? GameMode { get; set; }
    }
}