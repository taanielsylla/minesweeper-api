using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ee.itcollege.tasull.Contracts.Domain;

namespace BLL.App.DTO
{
    public class CurrencyType : IDomainEntityId
    {
        public Guid Id { get; set; }

        [MaxLength(512)] public string Name { get; set; } = default!;

        public ICollection<Configuration>? Configurations { get; set; }
        public ICollection<Wallet>? Wallets { get; set; }
        public ICollection<Transaction>? Transactions { get; set; }
        public ICollection<Faucet>? Faucets { get; set; }
    }
}