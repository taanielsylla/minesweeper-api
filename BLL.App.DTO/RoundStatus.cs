using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ee.itcollege.tasull.Contracts.Domain;

namespace BLL.App.DTO
{
    public class RoundStatus : IDomainEntityId
    {
        public Guid Id { get; set; }

        [MaxLength(512)] public string Name { get; set; } = default!;

        public ICollection<GameRound>? GameRounds { get; set; }
    }
}