﻿using System;
using BLL.App.DTO.Identity;
using ee.itcollege.tasull.Contracts.Domain;

namespace BLL.App.DTO
{
    public class Affiliate : IDomainEntityId
    {
        public Guid Id { get; set; }

        public Guid AppUserId { get; set; } // ID of user who referred
        public AppUser? AppUser { get; set; }

        public Guid ReferredTo { get; set; } // ID of user who was referred
    }
}