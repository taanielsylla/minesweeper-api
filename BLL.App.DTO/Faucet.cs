﻿using System;
using BLL.App.DTO.Identity;
using ee.itcollege.tasull.Contracts.Domain;

namespace BLL.App.DTO
{
    public class Faucet : IDomainEntityId
    {
        public Guid Id { get; set; }

        public Guid AppUserId { get; set; }
        public AppUser? AppUser { get; set; }

        public Guid CurrencyTypeId { get; set; }
        public CurrencyType? CurrencyType { get; set; }

        public double AmountRetrieved { get; set; }
    }
}