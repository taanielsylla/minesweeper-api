using System;
using System.ComponentModel.DataAnnotations;
using ee.itcollege.tasull.Contracts.Domain;

namespace PublicApi.DTO.v1
{
    public class TransactionType : IDomainEntityId
    {
        public Guid Id { get; set; }

        [MaxLength(512)] public string Name { get; set; } = default!;
    }
}