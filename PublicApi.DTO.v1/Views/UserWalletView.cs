﻿namespace PublicApi.DTO.v1.Views
{
    public class UserWalletView
    {
        public string? CurrencyName { get; set; }

        public double? Amount { get; set; }
    }
}