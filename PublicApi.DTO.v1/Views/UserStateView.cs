﻿using System;
using System.Collections.Generic;
using PublicApi.DTO.v1.Views;

namespace PublicApi.DTO.v1
{
    public class UserStateView
    {
        public string? NickName { get; set; }

        public string? Status { get; set; }

        public double? Balance { get; set; }

        public string? CurrencyType { get; set; }

        public Guid? ConfigurationId { get; set; }

        public int? CurrentLevel { get; set; }

        public double? Win { get; set; }

        public double? TotalWin { get; set; } // win from each round in total

        public IList<UserWalletView>? Wallets { get; set; }
    }
}