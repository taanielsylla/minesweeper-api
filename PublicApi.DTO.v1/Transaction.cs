﻿using System;
using ee.itcollege.tasull.Contracts.Domain;

namespace PublicApi.DTO.v1
{
    public class Transaction : IDomainEntityId
    {
        public Guid Id { get; set; }

        public Guid AppUserId { get; set; }

        public Guid CurrencyTypeId { get; set; }

        public Guid TransactionStatusId { get; set; }

        public Guid TransactionTypeId { get; set; }

        public double? Amount { get; set; }
        public double? EndingBalance { get; set; }
    }
}