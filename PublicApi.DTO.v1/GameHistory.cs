﻿using System;
using ee.itcollege.tasull.Contracts.Domain;

namespace PublicApi.DTO.v1
{
    public class GameHistory : IDomainEntityId
    {
        public Guid Id { get; set; }

        public Guid AppUserId { get; set; }
        
        public Guid GameRoundId { get; set; }

        public double? Bet { get; set; }
        public double? Win { get; set; }
    }
}