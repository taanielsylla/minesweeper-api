﻿using System;
using ee.itcollege.tasull.Contracts.Domain;

namespace PublicApi.DTO.v1
{
    public class Faucet : IDomainEntityId
    {
        public Guid Id { get; set; }

        public Guid AppUserId { get; set; }

        public Guid CurrencyTypeId { get; set; }

        public double AmountRetrieved { get; set; }
    }
}