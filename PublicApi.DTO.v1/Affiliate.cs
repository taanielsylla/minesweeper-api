﻿using System;
using ee.itcollege.tasull.Contracts.Domain;

namespace PublicApi.DTO.v1
{
    public class Affiliate : IDomainEntityId
    {
        public Guid Id { get; set; }

        public Guid AppUserId { get; set; } // ID of user who referred

        public Guid ReferredTo { get; set; } // ID of user who was referred
    }
}