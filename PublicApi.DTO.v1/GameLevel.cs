﻿using System;
using ee.itcollege.tasull.Contracts.Domain;

namespace PublicApi.DTO.v1
{
    public class GameLevel : IDomainEntityId
    {
        public Guid Id { get; set; }

        public Guid GameModeId { get; set; }

        public double Bet { get; set; }
        public int? LevelNr { get; set; }
        public int? BoxCount { get; set; }
    }
}