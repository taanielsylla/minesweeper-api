﻿using System;
using ee.itcollege.tasull.Contracts.Domain;

namespace PublicApi.DTO.v1
{
    public class GameRound : IDomainEntityId
    {
        public Guid Id { get; set; }

        public Guid AppUserId { get; set; }

        public Guid RoundStatusId { get; set; }

        public Guid ConfigurationId { get; set; }

        public int CurrentLevel { get; set; }
    }
}