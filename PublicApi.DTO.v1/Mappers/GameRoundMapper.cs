using AutoMapper;
using PublicApi.DTO.v1.Views;

namespace PublicApi.DTO.v1.Mappers
{
    public class GameRoundMapper : BaseMapper<BLL.App.DTO.GameRound, GameRound>
    {
        public GameRoundMapper()
        {
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.Views.StateView, StateView>();
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.Views.UserStateView, UserStateView>();

            MapperConfigurationExpression.CreateMap<BLL.App.DTO.Views.ConfigurationView, ConfigurationView>();
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.Views.HistoryView, HistoryView>();
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.Views.UserWalletView, UserWalletView>();


            Mapper = new Mapper(new MapperConfiguration(MapperConfigurationExpression));
        }

        public StateView MapStateView(BLL.App.DTO.Views.StateView inObject)
        {
            return Mapper.Map<StateView>(inObject);
        }

        public UserStateView MapUserStateView(BLL.App.DTO.Views.UserStateView inObject)
        {
            return Mapper.Map<UserStateView>(inObject);
        }
    }
}