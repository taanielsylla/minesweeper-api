﻿using System;
using ee.itcollege.tasull.Contracts.Domain;

namespace PublicApi.DTO.v1
{
    public class Wallet : IDomainEntityId
    {
        public Guid Id { get; set; }

        public Guid AppUserId { get; set; }

        public Guid CurrencyTypeId { get; set; }

        public double Amount { get; set; }
    }
}