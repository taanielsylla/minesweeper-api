﻿using System;
using System.ComponentModel.DataAnnotations;
using ee.itcollege.tasull.Contracts.Domain;

namespace PublicApi.DTO.v1
{
    public class Configuration : IDomainEntityId
    {
        public Guid Id { get; set; }

        public Guid CurrencyTypeId { get; set; }

        [MaxLength(128)] [MinLength(1)] public string Name { get; set; } = default!;

        public Guid GameModeId { get; set; }
    }
}