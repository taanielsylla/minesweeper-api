﻿using System;
using ee.itcollege.tasull.Contracts.Domain;

namespace DAL.App.DTO
{
    public class GameLevel : IDomainEntityId
    {
        public Guid Id { get; set; }

        public Guid GameModeId { get; set; }
        public GameMode? GameMode { get; set; }

        public double Bet { get; set; }
        public int? LevelNr { get; set; }
        public int? BoxCount { get; set; }
    }
}