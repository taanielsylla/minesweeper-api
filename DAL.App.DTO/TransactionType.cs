using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ee.itcollege.tasull.Contracts.Domain;

namespace DAL.App.DTO
{
    public class TransactionType : IDomainEntityId
    {
        public Guid Id { get; set; }

        [MaxLength(512)] public string Name { get; set; } = default!;
        public ICollection<Transaction>? Transactions { get; set; }
    }
}