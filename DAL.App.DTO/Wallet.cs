﻿using System;
using DAL.App.DTO.Identity;
using ee.itcollege.tasull.Contracts.Domain;

namespace DAL.App.DTO
{
    public class Wallet : IDomainEntityId
    {
        public Guid Id { get; set; }

        public Guid AppUserId { get; set; }
        public AppUser? AppUser { get; set; }

        public Guid CurrencyTypeId { get; set; }
        public CurrencyType? CurrencyType { get; set; }

        public double Amount { get; set; }
    }
}