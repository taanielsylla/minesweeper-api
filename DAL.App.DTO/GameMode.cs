﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ee.itcollege.tasull.Contracts.Domain;

namespace DAL.App.DTO
{
    public class GameMode : IDomainEntityId
    {
        public Guid Id { get; set; }

        [MaxLength(512)] public string Name { get; set; } = default!;

        public int? CountOfLevels { get; set; }

        public ICollection<GameLevel>? GameLevels { get; set; }
        public ICollection<Configuration>? Configurations { get; set; }
    }
}