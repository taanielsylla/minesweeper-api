﻿namespace DAL.App.DTO.Views
{
    public class HistoryView
    {
        public string? NickName { get; set; }

        public double? Bet { get; set; }

        public double? Win { get; set; }
    }
}