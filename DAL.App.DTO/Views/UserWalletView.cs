﻿namespace DAL.App.DTO.Views
{
    public class UserWalletView
    {
        public string? CurrencyName { get; set; }

        public double? Amount { get; set; }
    }
}