﻿using System;
using DAL.App.DTO.Identity;
using ee.itcollege.tasull.Contracts.Domain;

namespace DAL.App.DTO
{
    public class GameRound : IDomainEntityId
    {
        public Guid Id { get; set; }

        public Guid AppUserId { get; set; }
        public AppUser? AppUser { get; set; }

        public Guid RoundStatusId { get; set; }
        public RoundStatus? RoundStatus { get; set; }

        public Guid ConfigurationId { get; set; }
        public Configuration? Configuration { get; set; }

        public int CurrentLevel { get; set; }
    }
}