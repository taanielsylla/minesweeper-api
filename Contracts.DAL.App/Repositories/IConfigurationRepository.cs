using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ee.itcollege.tasull.Contracts.DAL.@base.Repositories;
using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IConfigurationRepository : IBaseRepository<Configuration>, IConfigurationRepositoryCustom
    {
        Task<double?> GetLevelBet(Guid gameModeId, int levelNr);
        Task<double> GetLevelsCount(Guid gameModeId);
    }
}