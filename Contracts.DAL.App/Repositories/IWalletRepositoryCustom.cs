using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IWalletRepositoryCustom : IWalletRepositoryCustom<Wallet>
    {
    }

    public interface IWalletRepositoryCustom<TWallet>
    {
    }
}