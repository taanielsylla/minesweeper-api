using ee.itcollege.tasull.Contracts.DAL.@base.Repositories;
using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IGameLevelRepository : IBaseRepository<GameLevel>, IGameLevelRepositoryCustom
    {
    }
}