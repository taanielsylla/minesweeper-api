using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IGameRoundRepositoryCustom : IGameRoundRepositoryCustom<GameRound>
    {
    }

    public interface IGameRoundRepositoryCustom<TGameRound>
    {
    }
}