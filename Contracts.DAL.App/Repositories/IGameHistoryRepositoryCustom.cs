using System.Collections.Generic;
using System.Threading.Tasks;
using DAL.App.DTO;
using DAL.App.DTO.Views;

namespace Contracts.DAL.App.Repositories
{
    public interface IGameHistoryRepositoryCustom : IGameHistoryRepositoryCustom<GameHistory>
    {
    }

    public interface IGameHistoryRepositoryCustom<TGameHistory>
    {
        Task<IEnumerable<HistoryView>> GetLastNEntries(int count);
    }
}