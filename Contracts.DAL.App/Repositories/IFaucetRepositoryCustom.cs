using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IFaucetRepositoryCustom : IFaucetRepositoryCustom<Faucet>
    {
    }

    public interface IFaucetRepositoryCustom<TFaucet>
    {
    }
}