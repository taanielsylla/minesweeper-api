using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IAffiliateRepositoryCustom : IAffiliateRepositoryCustom<Affiliate>
    {
    }

    public interface IAffiliateRepositoryCustom<TAffiliate>
    {
    }
}