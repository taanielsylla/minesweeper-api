using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface ICurrencyTypeRepositoryCustom : ICurrencyTypeRepositoryCustom<CurrencyType>
    {
    }

    public interface ICurrencyTypeRepositoryCustom<TCurrencyType>
    {
    }
}