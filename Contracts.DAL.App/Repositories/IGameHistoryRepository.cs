using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ee.itcollege.tasull.Contracts.DAL.@base.Repositories;
using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IGameHistoryRepository : IBaseRepository<GameHistory>, IGameHistoryRepositoryCustom
    {
        Task<IEnumerable<GameHistory>> GetRoundHistoryAsync(Guid roundId);
        Task<double> GetRoundTotalWin(Guid roundId);
        Task<double> GetRoundLastWin(Guid roundId);
        public Task<double> GetRoundTotalBet(Guid roundId);
    }
}