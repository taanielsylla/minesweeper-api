using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IRoundStatusRepositoryCustom : IRoundStatusRepositoryCustom<RoundStatus>
    {
    }

    public interface IRoundStatusRepositoryCustom<TRoundStatus>
    {
    }
}