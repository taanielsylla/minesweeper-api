using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface ITransactionRepositoryCustom : ITransactionRepositoryCustom<Transaction>
    {
    }

    public interface ITransactionRepositoryCustom<TTransaction>
    {
    }
}