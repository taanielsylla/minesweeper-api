using System;
using System.Threading.Tasks;
using ee.itcollege.tasull.Contracts.DAL.@base.Repositories;
using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IGameRoundRepository : IBaseRepository<GameRound>, IGameRoundRepositoryCustom
    {
        Task<GameRound?> GetLastActiveRound(Guid userId);
        Task<GameRound?> GetGenericUserGameData(Guid userId);
        Task UpdateRoundAfterBet(Guid id, int newLevel, Guid newStatus);
    }
}