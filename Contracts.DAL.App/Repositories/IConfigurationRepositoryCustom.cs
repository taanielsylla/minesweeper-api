using System.Collections.Generic;
using System.Threading.Tasks;
using DAL.App.DTO.Views;

namespace Contracts.DAL.App.Repositories
{
    public interface IConfigurationRepositoryCustom : IConfigurationRepositoryCustom<ConfigurationView>
    {
    }

    public interface IConfigurationRepositoryCustom<TConfigurationView>
    {
        Task<IEnumerable<TConfigurationView>> GetAllForViewAsync();
    }
}