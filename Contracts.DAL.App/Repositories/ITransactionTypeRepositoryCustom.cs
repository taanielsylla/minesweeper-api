using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface ITransactionTypeRepositoryCustom : ITransactionTypeRepositoryCustom<TransactionType>
    {
    }

    public interface ITransactionTypeRepositoryCustom<TTransactionType>
    {
    }
}