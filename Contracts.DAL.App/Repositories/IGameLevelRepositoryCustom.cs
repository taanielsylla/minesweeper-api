using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IGameLevelRepositoryCustom : IGameLevelRepositoryCustom<GameLevel>
    {
    }

    public interface IGameLevelRepositoryCustom<TGameLevel>
    {
    }
}