using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ee.itcollege.tasull.Contracts.DAL.@base.Repositories;
using DAL.App.DTO;
using DAL.App.DTO.Views;

namespace Contracts.DAL.App.Repositories
{
    public interface IWalletRepository : IBaseRepository<Wallet>, IWalletRepositoryCustom
    {
        public Task<Wallet?> GetWalletByCurrency(Guid currencyId, Guid userId);
        public Task<Wallet> ReduceWalletAmount(Guid currencyId, double amount, Guid userId);
        public Task<Wallet> IncreaseWalletAmount(Guid currencyId, double amount, Guid userId);
        public Task<List<UserWalletView>> GetUserWallets(Guid userId);
    }
}