using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface ITransactionStatusRepositoryCustom : ITransactionStatusRepositoryCustom<TransactionStatus>
    {
    }

    public interface ITransactionStatusRepositoryCustom<TTransactionStatus>
    {
    }
}