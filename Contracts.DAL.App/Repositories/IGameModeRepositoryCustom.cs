using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IGameModeRepositoryCustom : IGameModeRepositoryCustom<GameMode>
    {
    }

    public interface IGameModeRepositoryCustom<TGameMode>
    {
    }
}