﻿using System;
using Contracts.DAL.App.Repositories;
using ee.itcollege.tasull.Contracts.DAL.@base;

namespace Contracts.DAL.App
{
    public interface IAppUnitOfWork : IBaseUnitOfWork, IBaseEntityTracker
    {
        ILangStrRepository LangStrs { get; }
        ILangStrTranslationRepository LangStrTranslations { get; }

        IAffiliateRepository Affiliates { get; }
        IConfigurationRepository Configurations { get; }
        ICurrencyTypeRepository CurrencyTypes { get; }
        IFaucetRepository Faucets { get; }
        IGameHistoryRepository GameHistories { get; }
        IGameLevelRepository GameLevels { get; }
        IGameModeRepository GameModes { get; }
        IGameRoundRepository GameRounds { get; }
        IRoundStatusRepository RoundStatuses { get; }
        ITransactionRepository Transactions { get; }
        ITransactionStatusRepository TransactionStatuses { get; }
        ITransactionTypeRepository TransactionTypes { get; }
        IWalletRepository Wallets { get; }
    }
}