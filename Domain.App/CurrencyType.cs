using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ee.itcollege.tasull.Domain.@base;

namespace Domain.App
{
    public class CurrencyType : DomainEntityIdMetadata
    {
        [MaxLength(512)] public string Name { get; set; } = default!;

        public ICollection<Configuration>? Configurations { get; set; }
        public ICollection<Wallet>? Wallets { get; set; }
        public ICollection<Transaction>? Transactions { get; set; }
        public ICollection<Faucet>? Faucets { get; set; }
    }
}