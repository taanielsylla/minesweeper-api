﻿using System;
using Domain.App.Identity;
using ee.itcollege.tasull.Domain.@base;

namespace Domain.App
{
    public class Affiliate : DomainEntityIdMetadataUser<AppUser>
    {
        // public Guid AppUserId { get; set; } // ID of user who referred (this will be set after insertion into DB)
        public Guid ReferredTo { get; set; } // ID of user who was referred
    }
}