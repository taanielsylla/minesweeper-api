﻿using System;
using ee.itcollege.tasull.Domain.@base;

namespace Domain.App
{
    public class GameLevel : DomainEntityIdMetadata
    {
        public Guid GameModeId { get; set; }
        public GameMode? GameMode { get; set; }

        public double Bet { get; set; }
        public int? LevelNr { get; set; }
        public int? BoxCount { get; set; }
    }
}