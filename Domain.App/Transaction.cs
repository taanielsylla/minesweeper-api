﻿using System;
using Domain.App.Identity;
using ee.itcollege.tasull.Domain.@base;

namespace Domain.App
{
    public class Transaction : DomainEntityIdMetadataUser<AppUser>
    {
        public Guid CurrencyTypeId { get; set; }
        public CurrencyType? CurrencyType { get; set; }

        public Guid TransactionStatusId { get; set; }
        public TransactionStatus? TransactionStatus { get; set; }

        public Guid TransactionTypeId { get; set; }
        public TransactionType? TransactionType { get; set; }

        public double? Amount { get; set; }
        public double? EndingBalance { get; set; }
    }
}