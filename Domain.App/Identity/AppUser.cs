using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ee.itcollege.tasull.Contracts.Domain;
using Microsoft.AspNetCore.Identity;

namespace Domain.App.Identity
{
    public class AppUser : IdentityUser<Guid>, IDomainEntityId
    {
        [MinLength(1)]
        [MaxLength(128)]
        [Required]
        public string FirstName { get; set; } = default!;

        [MinLength(1)]
        [MaxLength(128)]
        [Required]
        public string LastName { get; set; } = default!;

        public string FirstLastName => FirstName + " " + LastName;
        public string LastFirstName => LastName + " " + FirstName;


        [MinLength(1)]
        [MaxLength(128)]
        [Required]
        public string? Nickname { get; set; } = default!;

        public ICollection<Transaction>? Transactions { get; set; }
        public ICollection<Wallet>? Wallets { get; set; }
        public ICollection<Faucet>? Faucets { get; set; }
        public ICollection<Affiliate>? Affiliates { get; set; }
        public ICollection<GameHistory>? GameHistories { get; set; }
        public ICollection<GameRound>? GameRounds { get; set; }
    }
}