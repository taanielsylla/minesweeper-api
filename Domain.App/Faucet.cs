﻿using System;
using Domain.App.Identity;
using ee.itcollege.tasull.Domain.@base;

namespace Domain.App
{
    public class Faucet : DomainEntityIdMetadataUser<AppUser>
    {
        public Guid CurrencyTypeId { get; set; }
        public CurrencyType? CurrencyType { get; set; }

        public double AmountRetrieved { get; set; }
    }
}