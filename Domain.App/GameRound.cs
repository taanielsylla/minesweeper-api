﻿using System;
using System.Collections.Generic;
using Domain.App.Identity;
using ee.itcollege.tasull.Domain.@base;

namespace Domain.App
{
    public class GameRound : DomainEntityIdMetadataUser<AppUser>
    {
        public Guid RoundStatusId { get; set; }
        public RoundStatus? RoundStatus { get; set; }

        public Guid ConfigurationId { get; set; }
        public Configuration? Configuration { get; set; }
        
        public int CurrentLevel { get; set; }
        
        public ICollection<GameHistory>? GameHistories { get; set; }
    }
}