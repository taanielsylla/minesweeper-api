﻿using System;
using Domain.App.Identity;
using ee.itcollege.tasull.Domain.@base;

namespace Domain.App
{
    public class Wallet : DomainEntityIdMetadataUser<AppUser>
    {
        public Guid CurrencyTypeId { get; set; }
        public CurrencyType? CurrencyType { get; set; }

        public double Amount { get; set; }
    }
}