﻿using System;
using Domain.App.Identity;
using ee.itcollege.tasull.Domain.@base;

namespace Domain.App
{
    public class GameHistory : DomainEntityIdMetadataUser<AppUser>
    {
        public Guid GameRoundId { get; set; }

        public GameRound? GameRound { get; set; }

        public double? Bet { get; set; }
        public double? Win { get; set; }
    }
}