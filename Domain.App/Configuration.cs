﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ee.itcollege.tasull.Domain.@base;

namespace Domain.App
{
    public class Configuration : DomainEntityIdMetadata
    {
        public Guid CurrencyTypeId { get; set; }
        public CurrencyType? CurrencyType { get; set; }

        [MaxLength(128)] [MinLength(1)] public string Name { get; set; } = default!;

        public ICollection<GameRound>? GameRounds { get; set; }

        public Guid GameModeId { get; set; }
        public GameMode? GameMode { get; set; }
    }
}