using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ee.itcollege.tasull.Domain.@base;

namespace Domain.App
{
    public class TransactionType : DomainEntityIdMetadata
    {
        [MaxLength(512)] public string Name { get; set; } = default!;
        public ICollection<Transaction>? Transactions { get; set; }
    }
}