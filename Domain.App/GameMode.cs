﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ee.itcollege.tasull.Domain.@base;

namespace Domain.App
{
    public class GameMode : DomainEntityIdMetadata
    {
        [MaxLength(512)] public string Name { get; set; } = default!;

        public int? CountOfLevels { get; set; }

        public ICollection<GameLevel>? GameLevels { get; set; }
        public ICollection<Configuration>? Configurations { get; set; }
    }
}