using System.Threading.Tasks;
using Contracts.BLL.App;
using Extensions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PublicApi.DTO.v1.Mappers;
using PublicApi.DTO.v1.Views;
using V1DTO = PublicApi.DTO.v1;

namespace WebApp.ApiControllers._1._0.ActionControllers
{
    /// <summary>
    /// Init - Starts a new round for the player, here we set the config and currency type for the round.
    /// User can start doing bets after round has been initialized.
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class InitController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly GameRoundMapper _mapper = new GameRoundMapper();

        /// <summary>
        /// Constructor
        /// </summary>
        public InitController(IAppBLL bll)
        {
            _bll = bll;
        }

        /// <summary>
        /// Initialize a new GameRound
        /// </summary>
        /// <param name="initRequest">config to start the round with</param>
        /// <returns>UserStateView object</returns>
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "user")]
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.UserStateView))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.UserStateView>> InitRound(InitRequestView initRequest)
        {
            if (initRequest.ConfigurationId == null)
            {
                return NotFound(new V1DTO.MessageDTO(
                    $"Please provide configurationId for the successful initialization of a round."));
            }

            var roundId = await _bll.GameRounds.InitRound(initRequest, User.UserId());
            if (roundId == null)
            {
                return NotFound(new V1DTO.MessageDTO(
                    $"Can't initialize new round for userId: {User.UserId()} with configurationId: {initRequest.ConfigurationId}, please check request fields or resolve possible active round!"));
            }

            var result = await _bll.GameRounds.GetUserStateViewAsync(User.UserId());
            return Ok(_mapper.MapUserStateView(result!));
        }
    }
}