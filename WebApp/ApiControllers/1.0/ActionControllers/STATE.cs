using System;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Extensions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PublicApi.DTO.v1.Mappers;
using V1DTO = PublicApi.DTO.v1;

namespace WebApp.ApiControllers._1._0.ActionControllers
{
    /// <summary>
    /// State - Record player state, return player progress on request.
    /// Let client application know if player has any active rounds or if game is over.
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class StateController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly GameRoundMapper _mapper = new GameRoundMapper();

        /// <summary>
        /// Constructor
        /// </summary>
        public StateController(IAppBLL bll)
        {
            _bll = bll;
        }

        /// <summary>
        /// Get specific user state, return data about active game, configuration and status.
        /// </summary>
        /// <returns>request GameRound</returns>
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "user")]
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.GameRound))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.GameRound>> GetState()
        {
            var bllUserState = await _bll.GameRounds.GetUserStateViewAsync(User.UserId());
            if (bllUserState == null)
            {
                return NotFound(
                    new V1DTO.MessageDTO("GameRound not found for specified user, please INIT a new round"));
            }

            return Ok(_mapper.MapUserStateView(bllUserState));
        }
    }
}