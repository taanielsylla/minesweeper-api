using System.Threading.Tasks;
using Contracts.BLL.App;
using Extensions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PublicApi.DTO.v1.Mappers;
using V1DTO = PublicApi.DTO.v1;

namespace WebApp.ApiControllers._1._0.ActionControllers
{
    /// <summary>
    /// Bet - Allows user to wager amount for a chance to win double the amount back. Round needs to be initialized and
    /// round status needs to be ACTIVE for betting to be successful. Calling BET without an active round will do nothing.
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class BetController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly GameRoundMapper _mapper = new GameRoundMapper();

        /// <summary>
        /// Constructor
        /// </summary>
        public BetController(IAppBLL bll)
        {
            _bll = bll;
        }

        /// <summary>
        /// Execute a BET action
        /// </summary>
        /// <returns>UserStateView object</returns>
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "user")]
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.UserStateView))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.UserStateView>> ExecuteBet()
        {
            var updatedStateView = await _bll.GameRounds.ExecuteBet(User.UserId());
            if (updatedStateView == null)
            {
                return NotFound(new V1DTO.MessageDTO(
                    $"There was an error executing BET for userId: {User.UserId()}, have you started a round ?!"));
            }

            return Ok(_mapper.MapUserStateView(updatedStateView));
        }
    }
}