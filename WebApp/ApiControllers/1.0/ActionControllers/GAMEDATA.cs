using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PublicApi.DTO.v1.Mappers;
using V1DTO = PublicApi.DTO.v1;

namespace WebApp.ApiControllers._1._0.ActionControllers
{
    /// <summary>
    /// GAMEDATA - Return data general data about game, any configurations, available currencies, leader-board etc.
    /// This endpoint is free for anonymous users to use. Is used for initial logged-out view creation on client
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [AllowAnonymous]
    public class GameDataController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly GameRoundMapper _mapper = new GameRoundMapper();

        /// <summary>
        /// Constructor
        /// </summary>
        public GameDataController(IAppBLL bll)
        {
            _bll = bll;
        }

        /// <summary>
        /// Get generic state, this is to show initial game without user having to login.
        /// </summary>
        /// <returns>Generic game state with configurations</returns>
        [HttpGet]
        [AllowAnonymous]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<V1DTO.StateView>))]
        public async Task<ActionResult<IEnumerable<V1DTO.StateView>>> GetState()
        {
            var bllState = await _bll.GameRounds.GetStateViewAsync();

            return Ok(_mapper.MapStateView(bllState));
        }
    }
}