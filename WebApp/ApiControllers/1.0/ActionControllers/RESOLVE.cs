using System.Threading.Tasks;
using Contracts.BLL.App;
using Extensions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PublicApi.DTO.v1.Mappers;
using V1DTO = PublicApi.DTO.v1;

namespace WebApp.ApiControllers._1._0.ActionControllers
{
    /// <summary>
    /// Resolve - Resolve a users active round. If user has won anything already, then calling this will withdraw
    /// winnings and conclude game round. If user has no active rounds, then calling this will do nothing.
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class ResolveController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly GameRoundMapper _mapper = new GameRoundMapper();

        /// <summary>
        /// Constructor
        /// </summary>
        public ResolveController(IAppBLL bll)
        {
            _bll = bll;
        }

        /// <summary>
        /// Execute a RESOLVE action.
        /// This will resolve the round, not letting the player to continue to make BET actions on it.
        /// Resolve will take any winnings that have been won so far, and close the game round.
        /// A player is not allowed to RESOLVE a gameRound that has a level of 0 (no bets made during round)
        /// </summary>
        /// <returns>UserStateView object</returns>
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "user")]
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.UserStateView))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.UserStateView>> ResolveRound()
        {
            var updatedStateView = await _bll.GameRounds.ResolveRound(User.UserId());
            if (updatedStateView == null)
            {
                return NotFound(new V1DTO.MessageDTO(
                    $"There was an error executing RESOLVE for userId: {User.UserId()}, are you sure player has an ACTIVE round ?!"));
            }

            return Ok(_mapper.MapUserStateView(updatedStateView));
        }
    }
}