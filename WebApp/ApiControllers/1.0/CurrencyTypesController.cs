using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using PublicApi.DTO.v1.Mappers;
using V1DTO = PublicApi.DTO.v1;

namespace WebApp.ApiControllers._1._0
{
    /// <summary>
    /// Currency types - BitCoin, Satoshi, MilliBitcoin, 
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class CurrencyTypesController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly CurrencyTypeMapper _mapper = new CurrencyTypeMapper();

        /// <summary>
        /// Constructor
        /// </summary>
        public CurrencyTypesController(IAppBLL bll)
        {
            _bll = bll;
        }

        /// <summary>
        /// Get the predefined CurrencyType collection.
        /// Non-fiat currencies, BTC, Mbtc, satoshi
        /// </summary>
        /// <returns>List of available CurrencyTypes</returns>
        [HttpGet]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<V1DTO.CurrencyType>))]
        public async Task<ActionResult<IEnumerable<V1DTO.CurrencyType>>> GetCurrencyTypes()
        {
            return Ok((await _bll.CurrencyTypes.GetAllAsync()).Select(e => _mapper.Map(e)));
        }


        /// <summary>
        /// Get single CurrencyType
        /// </summary>
        /// <param name="id">CurrencyType Id</param>
        /// <returns>request CurrencyType</returns>
        [HttpGet("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.CurrencyType))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.CurrencyType>> GetCurrencyType(Guid id)
        {
            var currencyType = await _bll.CurrencyTypes.FirstOrDefaultAsync(id);

            if (currencyType == null)
            {
                return NotFound(new V1DTO.MessageDTO("CurrencyType not found"));
            }

            return Ok(_mapper.Map(currencyType));
        }

        /// <summary>
        /// Update the CurrencyType
        /// </summary>
        /// <param name="id">CurrencyType id</param>
        /// <param name="currencyType">CurrencyType object</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(V1DTO.MessageDTO))]
        public async Task<IActionResult> PutCurrencyType(Guid id, V1DTO.CurrencyType currencyType)
        {
            if (id != currencyType.Id)
            {
                return BadRequest(new V1DTO.MessageDTO("id and currencyType.id do not match"));
            }

            await _bll.CurrencyTypes.UpdateAsync(_mapper.Map(currencyType));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Create a new CurrencyType
        /// </summary>
        /// <param name="currencyType">CurrencyType object</param>
        /// <returns>created CurrencyType object</returns>
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(V1DTO.CurrencyType))]
        public async Task<ActionResult<V1DTO.CurrencyType>> PostCurrencyType(
            V1DTO.CurrencyType currencyType)
        {
            var bllEntity = _mapper.Map(currencyType);
            _bll.CurrencyTypes.Add(bllEntity);
            await _bll.SaveChangesAsync();
            currencyType.Id = bllEntity.Id;

            return CreatedAtAction("GetCurrencyType",
                new {id = currencyType.Id, version = HttpContext.GetRequestedApiVersion()?.ToString() ?? "0"},
                currencyType);
        }

        /// <summary>
        /// Delete the CurrencyType
        /// </summary>
        /// <param name="id">CurrencyType Id</param>
        /// <returns>deleted CurrencyType object</returns>
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.CurrencyType))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.CurrencyType>> DeleteCurrencyType(Guid id)
        {
            var currencyType = await _bll.CurrencyTypes.FirstOrDefaultAsync(id);
            if (currencyType == null)
            {
                return NotFound(new V1DTO.MessageDTO("CurrencyType not found"));
            }

            await _bll.CurrencyTypes.RemoveAsync(currencyType);
            await _bll.SaveChangesAsync();

            return Ok(currencyType);
        }
    }
}