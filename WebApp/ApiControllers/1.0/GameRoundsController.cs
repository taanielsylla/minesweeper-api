using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using PublicApi.DTO.v1.Mappers;
using V1DTO = PublicApi.DTO.v1;

namespace WebApp.ApiControllers._1._0
{
    /// <summary>
    /// GameRounds - Keep track of user game round, see current state of round, is it active/resolved.
    /// A user can have one active round for one type of currency, a user can have game rounds active for different currencies.
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class GameRoundsController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly GameRoundMapper _mapper = new GameRoundMapper();

        /// <summary>
        /// Constructor
        /// </summary>
        public GameRoundsController(IAppBLL bll)
        {
            _bll = bll;
        }

        /// <summary>
        /// Get the GameRound collection.
        /// All records of GameRounds
        /// </summary>
        /// <returns>List of all gameRounds</returns>
        [HttpGet]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<V1DTO.GameRound>))]
        public async Task<ActionResult<IEnumerable<V1DTO.GameRound>>> GetGameRounds()
        {
            return Ok((await _bll.GameRounds.GetAllAsync()).Select(e => _mapper.Map(e)));
        }


        /// <summary>
        /// Get single GameRound
        /// </summary>
        /// <param name="id">GameRound Id</param>
        /// <returns>request GameRound</returns>
        [HttpGet("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.GameRound))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.GameRound>> GetGameRound(Guid id)
        {
            var gameRound = await _bll.GameRounds.FirstOrDefaultAsync(id);

            if (gameRound == null)
            {
                return NotFound(new V1DTO.MessageDTO("GameRound not found"));
            }

            return Ok(_mapper.Map(gameRound));
        }

        /// <summary>
        /// Update the GameRound
        /// </summary>
        /// <param name="id">GameRound id</param>
        /// <param name="gameRound">GameRound object</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(V1DTO.MessageDTO))]
        public async Task<IActionResult> PutGameRound(Guid id, V1DTO.GameRound gameRound)
        {
            if (id != gameRound.Id)
            {
                return BadRequest(new V1DTO.MessageDTO("id and gameRound.id do not match"));
            }

            await _bll.GameRounds.UpdateAsync(_mapper.Map(gameRound));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Create a new GameRound
        /// </summary>
        /// <param name="gameRound">GameRound object</param>
        /// <returns>created GameRound object</returns>
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(V1DTO.GameRound))]
        public async Task<ActionResult<V1DTO.GameRound>> PostGameRound(V1DTO.GameRound gameRound)
        {
            var bllEntity = _mapper.Map(gameRound);
            _bll.GameRounds.Add(bllEntity);
            await _bll.SaveChangesAsync();
            gameRound.Id = bllEntity.Id;

            return CreatedAtAction("GetGameRound",
                new {id = gameRound.Id, version = HttpContext.GetRequestedApiVersion()?.ToString() ?? "0"},
                gameRound);
        }

        /// <summary>
        /// Delete the GameRound
        /// </summary>
        /// <param name="id">GameRound Id</param>
        /// <returns>deleted GameRound object</returns>
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.GameRound))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.GameRound>> DeleteGameRound(Guid id)
        {
            var gameRound = await _bll.GameRounds.FirstOrDefaultAsync(id);
            if (gameRound == null)
            {
                return NotFound(new V1DTO.MessageDTO("GameRound not found"));
            }

            await _bll.GameRounds.RemoveAsync(gameRound);
            await _bll.SaveChangesAsync();

            return Ok(gameRound);
        }
    }
}