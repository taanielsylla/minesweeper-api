using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using PublicApi.DTO.v1.Mappers;
using V1DTO = PublicApi.DTO.v1;

namespace WebApp.ApiControllers._1._0
{
    /// <summary>
    /// Faucets - Stores user faucet usage data, to see how many times user has used faucet, limit for 30 times of a small amount.
    /// Enforce 30 min delay between usages. 
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class FaucetsController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly FaucetMapper _mapper = new FaucetMapper();

        /// <summary>
        /// Constructor
        /// </summary>
        public FaucetsController(IAppBLL bll)
        {
            _bll = bll;
        }

        /// <summary>
        /// Get the predefined Faucet collection.
        /// All records of faucet usages
        /// </summary>
        /// <returns>List of all faucet records</returns>
        [HttpGet]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<V1DTO.Faucet>))]
        public async Task<ActionResult<IEnumerable<V1DTO.Faucet>>> GetFaucets()
        {
            return Ok((await _bll.Faucets.GetAllAsync()).Select(e => _mapper.Map(e)));
        }


        /// <summary>
        /// Get single Faucet record
        /// </summary>
        /// <param name="id">Faucet Id</param>
        /// <returns>request Faucet</returns>
        [HttpGet("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.Faucet))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.Faucet>> GetFaucet(Guid id)
        {
            var faucet = await _bll.Faucets.FirstOrDefaultAsync(id);

            if (faucet == null)
            {
                return NotFound(new V1DTO.MessageDTO("Faucet not found"));
            }

            return Ok(_mapper.Map(faucet));
        }

        /// <summary>
        /// Update the Faucet record
        /// </summary>
        /// <param name="id">Faucet id</param>
        /// <param name="faucet">Faucet object</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(V1DTO.MessageDTO))]
        public async Task<IActionResult> PutFaucet(Guid id, V1DTO.Faucet faucet)
        {
            if (id != faucet.Id)
            {
                return BadRequest(new V1DTO.MessageDTO("id and faucet.id do not match"));
            }

            await _bll.Faucets.UpdateAsync(_mapper.Map(faucet));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Create a new Faucet
        /// </summary>
        /// <param name="faucet">Faucet object</param>
        /// <returns>created Faucet object</returns>
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(V1DTO.Faucet))]
        public async Task<ActionResult<V1DTO.Faucet>> PostFaucet(V1DTO.Faucet faucet)
        {
            var bllEntity = _mapper.Map(faucet);
            _bll.Faucets.Add(bllEntity);
            await _bll.SaveChangesAsync();
            faucet.Id = bllEntity.Id;

            return CreatedAtAction("GetFaucet",
                new {id = faucet.Id, version = HttpContext.GetRequestedApiVersion()?.ToString() ?? "0"},
                faucet);
        }

        /// <summary>
        /// Delete the Faucet
        /// </summary>
        /// <param name="id">Faucet Id</param>
        /// <returns>deleted Faucet object</returns>
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.Faucet))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.Faucet>> DeleteFaucet(Guid id)
        {
            var faucet = await _bll.Faucets.FirstOrDefaultAsync(id);
            if (faucet == null)
            {
                return NotFound(new V1DTO.MessageDTO("Faucet not found"));
            }

            await _bll.Faucets.RemoveAsync(faucet);
            await _bll.SaveChangesAsync();

            return Ok(faucet);
        }
    }
}