using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using PublicApi.DTO.v1.Mappers;
using V1DTO = PublicApi.DTO.v1;

namespace WebApp.ApiControllers._1._0
{
    /// <summary>
    /// GameHistories - Records metadata or other necessary data of user actions. 
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class GameHistoriesController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly GameHistoryMapper _mapper = new GameHistoryMapper();

        /// <summary>
        /// Constructor
        /// </summary>
        public GameHistoriesController(IAppBLL bll)
        {
            _bll = bll;
        }

        /// <summary>
        /// Get the predefined GameHistory collection.
        /// All records of user actions in GameHistory table
        /// </summary>
        /// <returns>List of all gameHistories</returns>
        [HttpGet]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<V1DTO.GameHistory>))]
        public async Task<ActionResult<IEnumerable<V1DTO.GameHistory>>> GetGameHistories()
        {
            return Ok((await _bll.GameHistories.GetAllAsync()).Select(e => _mapper.Map(e)));
        }


        /// <summary>
        /// Get single GameHistory
        /// </summary>
        /// <param name="id">GameHistory Id</param>
        /// <returns>request GameHistory</returns>
        [HttpGet("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.GameHistory))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.GameHistory>> GetGameHistory(Guid id)
        {
            var gameHistory = await _bll.GameHistories.FirstOrDefaultAsync(id);

            if (gameHistory == null)
            {
                return NotFound(new V1DTO.MessageDTO("GameHistory not found"));
            }

            return Ok(_mapper.Map(gameHistory));
        }

        /// <summary>
        /// Update the GameHistory
        /// </summary>
        /// <param name="id">GameHistory id</param>
        /// <param name="gameHistory">GameHistory object</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(V1DTO.MessageDTO))]
        public async Task<IActionResult> PutGameHistory(Guid id, V1DTO.GameHistory gameHistory)
        {
            if (id != gameHistory.Id)
            {
                return BadRequest(new V1DTO.MessageDTO("id and gameHistory.id do not match"));
            }

            await _bll.GameHistories.UpdateAsync(_mapper.Map(gameHistory));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Create a new GameHistory
        /// </summary>
        /// <param name="gameHistory">GameHistory object</param>
        /// <returns>created GameHistory object</returns>
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(V1DTO.GameHistory))]
        public async Task<ActionResult<V1DTO.GameHistory>> PostGameHistory(V1DTO.GameHistory gameHistory)
        {
            var bllEntity = _mapper.Map(gameHistory);
            _bll.GameHistories.Add(bllEntity);
            await _bll.SaveChangesAsync();
            gameHistory.Id = bllEntity.Id;

            return CreatedAtAction("GetGameHistory",
                new {id = gameHistory.Id, version = HttpContext.GetRequestedApiVersion()?.ToString() ?? "0"},
                gameHistory);
        }

        /// <summary>
        /// Delete the GameHistory
        /// </summary>
        /// <param name="id">GameHistory Id</param>
        /// <returns>deleted GameHistory object</returns>
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.GameHistory))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.GameHistory>> DeleteGameHistory(Guid id)
        {
            var gameHistory = await _bll.GameHistories.FirstOrDefaultAsync(id);
            if (gameHistory == null)
            {
                return NotFound(new V1DTO.MessageDTO("GameHistory not found"));
            }

            await _bll.GameHistories.RemoveAsync(gameHistory);
            await _bll.SaveChangesAsync();

            return Ok(gameHistory);
        }
    }
}