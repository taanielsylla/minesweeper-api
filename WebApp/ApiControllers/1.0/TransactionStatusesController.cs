using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using PublicApi.DTO.v1.Mappers;
using V1DTO = PublicApi.DTO.v1;

namespace WebApp.ApiControllers._1._0
{
    /// <summary>
    /// TransactionStatuses - All states that a transaction may have.
    /// Possible options - PENDING, FAILED, COMPLETED.  
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class TransactionStatusesController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly TransactionStatusMapper _mapper = new TransactionStatusMapper();

        /// <summary>
        /// Constructor
        /// </summary>
        public TransactionStatusesController(IAppBLL bll)
        {
            _bll = bll;
        }

        /// <summary>
        /// Get the predefined TransactionStatus collection.
        /// All records from the TransactionStatus table
        /// </summary>
        /// <returns>List of all transactionStatuses</returns>
        [HttpGet]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<V1DTO.TransactionStatus>))]
        public async Task<ActionResult<IEnumerable<V1DTO.TransactionStatus>>> GetTransactionStatuses()
        {
            return Ok((await _bll.TransactionStatuses.GetAllAsync()).Select(e => _mapper.Map(e)));
        }


        /// <summary>
        /// Get single TransactionStatus
        /// </summary>
        /// <param name="id">TransactionStatus Id</param>
        /// <returns>request TransactionStatus</returns>
        [HttpGet("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.TransactionStatus))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.TransactionStatus>> GetTransactionStatus(Guid id)
        {
            var transactionStatus = await _bll.TransactionStatuses.FirstOrDefaultAsync(id);

            if (transactionStatus == null)
            {
                return NotFound(new V1DTO.MessageDTO("TransactionStatus not found"));
            }

            return Ok(_mapper.Map(transactionStatus));
        }

        /// <summary>
        /// Update the TransactionStatus
        /// </summary>
        /// <param name="id">TransactionStatus id</param>
        /// <param name="transactionStatus">TransactionStatus object</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(V1DTO.MessageDTO))]
        public async Task<IActionResult> PutTransactionStatus(Guid id, V1DTO.TransactionStatus transactionStatus)
        {
            if (id != transactionStatus.Id)
            {
                return BadRequest(new V1DTO.MessageDTO("id and transactionStatus.id do not match"));
            }

            await _bll.TransactionStatuses.UpdateAsync(_mapper.Map(transactionStatus));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Create a new TransactionStatus
        /// </summary>
        /// <param name="transactionStatus">TransactionStatus object</param>
        /// <returns>created TransactionStatus object</returns>
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(V1DTO.TransactionStatus))]
        public async Task<ActionResult<V1DTO.TransactionStatus>> PostTransactionStatus(
            V1DTO.TransactionStatus transactionStatus)
        {
            var bllEntity = _mapper.Map(transactionStatus);
            _bll.TransactionStatuses.Add(bllEntity);
            await _bll.SaveChangesAsync();
            transactionStatus.Id = bllEntity.Id;

            return CreatedAtAction("GetTransactionStatus",
                new {id = transactionStatus.Id, version = HttpContext.GetRequestedApiVersion()?.ToString() ?? "0"},
                transactionStatus);
        }

        /// <summary>
        /// Delete the TransactionStatus
        /// </summary>
        /// <param name="id">TransactionStatus Id</param>
        /// <returns>deleted TransactionStatus object</returns>
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.TransactionStatus))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.TransactionStatus>> DeleteTransactionStatus(Guid id)
        {
            var transactionStatus = await _bll.TransactionStatuses.FirstOrDefaultAsync(id);
            if (transactionStatus == null)
            {
                return NotFound(new V1DTO.MessageDTO("TransactionStatus not found"));
            }

            await _bll.TransactionStatuses.RemoveAsync(transactionStatus);
            await _bll.SaveChangesAsync();

            return Ok(transactionStatus);
        }
    }
}