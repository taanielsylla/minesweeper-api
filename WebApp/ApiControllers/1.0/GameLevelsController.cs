using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using PublicApi.DTO.v1.Mappers;
using V1DTO = PublicApi.DTO.v1;

namespace WebApp.ApiControllers._1._0
{
    /// <summary>
    /// GameLevels - A level where player can place a bet and win an amount specified on the level.
    /// A level can have multiple horizontal boxes to click from. 
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class GameLevelsController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly GameLevelMapper _mapper = new GameLevelMapper();

        /// <summary>
        /// Constructor
        /// </summary>
        public GameLevelsController(IAppBLL bll)
        {
            _bll = bll;
        }

        /// <summary>
        /// Get the predefined GameLevel collection.
        /// All records from GameLevel table
        /// </summary>
        /// <returns>List of all gameLevels</returns>
        [HttpGet]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<V1DTO.GameLevel>))]
        public async Task<ActionResult<IEnumerable<V1DTO.GameLevel>>> GetGameLevels()
        {
            return Ok((await _bll.GameLevels.GetAllAsync()).Select(e => _mapper.Map(e)));
        }


        /// <summary>
        /// Get single GameLevel
        /// </summary>
        /// <param name="id">GameLevel Id</param>
        /// <returns>request GameLevel</returns>
        [HttpGet("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.GameLevel))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.GameLevel>> GetGameLevel(Guid id)
        {
            var gameLevel = await _bll.GameLevels.FirstOrDefaultAsync(id);

            if (gameLevel == null)
            {
                return NotFound(new V1DTO.MessageDTO("GameLevel not found"));
            }

            return Ok(_mapper.Map(gameLevel));
        }

        /// <summary>
        /// Update the GameLevel
        /// </summary>
        /// <param name="id">GameLevel id</param>
        /// <param name="gameLevel">GameLevel object</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(V1DTO.MessageDTO))]
        public async Task<IActionResult> PutGameLevel(Guid id, V1DTO.GameLevel gameLevel)
        {
            if (id != gameLevel.Id)
            {
                return BadRequest(new V1DTO.MessageDTO("id and gameLevel.id do not match"));
            }

            await _bll.GameLevels.UpdateAsync(_mapper.Map(gameLevel));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Create a new GameLevel
        /// </summary>
        /// <param name="gameLevel">GameLevel object</param>
        /// <returns>created GameLevel object</returns>
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(V1DTO.GameLevel))]
        public async Task<ActionResult<V1DTO.GameLevel>> PostGameLevel(V1DTO.GameLevel gameLevel)
        {
            var bllEntity = _mapper.Map(gameLevel);
            _bll.GameLevels.Add(bllEntity);
            await _bll.SaveChangesAsync();
            gameLevel.Id = bllEntity.Id;

            return CreatedAtAction("GetGameLevel",
                new {id = gameLevel.Id, version = HttpContext.GetRequestedApiVersion()?.ToString() ?? "0"},
                gameLevel);
        }

        /// <summary>
        /// Delete the GameLevel
        /// </summary>
        /// <param name="id">GameLevel Id</param>
        /// <returns>deleted GameLevel object</returns>
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.GameLevel))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.GameLevel>> DeleteGameLevel(Guid id)
        {
            var gameLevel = await _bll.GameLevels.FirstOrDefaultAsync(id);
            if (gameLevel == null)
            {
                return NotFound(new V1DTO.MessageDTO("GameLevel not found"));
            }

            await _bll.GameLevels.RemoveAsync(gameLevel);
            await _bll.SaveChangesAsync();

            return Ok(gameLevel);
        }
    }
}