using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using PublicApi.DTO.v1.Mappers;
using V1DTO = PublicApi.DTO.v1;

namespace WebApp.ApiControllers._1._0
{
    /// <summary>
    /// Configurations - Holds game configuration data, what currency used, what size bets at what levels are allowed etc. 
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class ConfigurationsController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly ConfigurationMapper _mapper = new ConfigurationMapper();

        /// <summary>
        /// Constructor
        /// </summary>
        public ConfigurationsController(IAppBLL bll)
        {
            _bll = bll;
        }

        /// <summary>
        /// Get the predefined Configuration collection.
        /// All records of Configuration, different game rounds
        /// </summary>
        /// <returns>List of all game configurations</returns>
        [HttpGet]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<V1DTO.Configuration>))]
        public async Task<ActionResult<IEnumerable<V1DTO.Configuration>>> GetConfigurations()
        {
            return Ok((await _bll.Configurations.GetAllAsync()).Select(e => _mapper.Map(e)));
        }


        /// <summary>
        /// Get single Configuration
        /// </summary>
        /// <param name="id">Configuration Id</param>
        /// <returns>request Configuration</returns>
        [HttpGet("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.Configuration))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.Configuration>> GetConfiguration(Guid id)
        {
            var configuration = await _bll.Configurations.FirstOrDefaultAsync(id);

            if (configuration == null)
            {
                return NotFound(new V1DTO.MessageDTO("Configuration not found"));
            }

            return Ok(_mapper.Map(configuration));
        }

        /// <summary>
        /// Update the Configuration
        /// </summary>
        /// <param name="id">Configuration id</param>
        /// <param name="configuration">Configuration object</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(V1DTO.MessageDTO))]
        public async Task<IActionResult> PutConfiguration(Guid id, V1DTO.Configuration configuration)
        {
            if (id != configuration.Id)
            {
                return BadRequest(new V1DTO.MessageDTO("id and configuration.id do not match"));
            }

            await _bll.Configurations.UpdateAsync(_mapper.Map(configuration));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Create a new Configuration
        /// </summary>
        /// <param name="configuration">Configuration object</param>
        /// <returns>created Configuration object</returns>
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(V1DTO.Configuration))]
        public async Task<ActionResult<V1DTO.Configuration>> PostConfiguration(V1DTO.Configuration configuration)
        {
            var bllEntity = _mapper.Map(configuration);
            _bll.Configurations.Add(bllEntity);
            await _bll.SaveChangesAsync();
            configuration.Id = bllEntity.Id;

            return CreatedAtAction("GetConfiguration",
                new {id = configuration.Id, version = HttpContext.GetRequestedApiVersion()?.ToString() ?? "0"},
                configuration);
        }

        /// <summary>
        /// Delete the Configuration
        /// </summary>
        /// <param name="id">Configuration Id</param>
        /// <returns>deleted Configuration object</returns>
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.Configuration))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.Configuration>> DeleteConfiguration(Guid id)
        {
            var configuration = await _bll.Configurations.FirstOrDefaultAsync(id);
            if (configuration == null)
            {
                return NotFound(new V1DTO.MessageDTO("Configuration not found"));
            }

            await _bll.Configurations.RemoveAsync(configuration);
            await _bll.SaveChangesAsync();

            return Ok(configuration);
        }
    }
}