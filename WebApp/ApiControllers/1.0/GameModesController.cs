using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using PublicApi.DTO.v1.Mappers;
using V1DTO = PublicApi.DTO.v1;

namespace WebApp.ApiControllers._1._0
{
    /// <summary>
    /// GameModes - Hard, medium, easy. Different modes have different level configurations (bet size, currencies). 
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class GameModesController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly GameModeMapper _mapper = new GameModeMapper();

        /// <summary>
        /// Constructor
        /// </summary>
        public GameModesController(IAppBLL bll)
        {
            _bll = bll;
        }

        /// <summary>
        /// Get the GameMode collection.
        /// All records of gameModes, for game configuration
        /// </summary>
        /// <returns>List of all gameModes</returns>
        [HttpGet]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<V1DTO.GameMode>))]
        public async Task<ActionResult<IEnumerable<V1DTO.GameMode>>> GetGameModes()
        {
            return Ok((await _bll.GameModes.GetAllAsync()).Select(e => _mapper.Map(e)));
        }


        /// <summary>
        /// Get single GameMode
        /// </summary>
        /// <param name="id">GameMode Id</param>
        /// <returns>request GameMode</returns>
        [HttpGet("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.GameMode))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.GameMode>> GetGameMode(Guid id)
        {
            var gameMode = await _bll.GameModes.FirstOrDefaultAsync(id);

            if (gameMode == null)
            {
                return NotFound(new V1DTO.MessageDTO("GameMode not found"));
            }

            return Ok(_mapper.Map(gameMode));
        }

        /// <summary>
        /// Update the GameMode
        /// </summary>
        /// <param name="id">GameMode id</param>
        /// <param name="gameMode">GameMode object</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(V1DTO.MessageDTO))]
        public async Task<IActionResult> PutGameMode(Guid id, V1DTO.GameMode gameMode)
        {
            if (id != gameMode.Id)
            {
                return BadRequest(new V1DTO.MessageDTO("id and gameMode.id do not match"));
            }

            await _bll.GameModes.UpdateAsync(_mapper.Map(gameMode));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Create a new GameMode
        /// </summary>
        /// <param name="gameMode">GameMode object</param>
        /// <returns>created GameMode object</returns>
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(V1DTO.GameMode))]
        public async Task<ActionResult<V1DTO.GameMode>> PostGameMode(V1DTO.GameMode gameMode)
        {
            var bllEntity = _mapper.Map(gameMode);
            _bll.GameModes.Add(bllEntity);
            await _bll.SaveChangesAsync();
            gameMode.Id = bllEntity.Id;

            return CreatedAtAction("GetGameMode",
                new {id = gameMode.Id, version = HttpContext.GetRequestedApiVersion()?.ToString() ?? "0"},
                gameMode);
        }

        /// <summary>
        /// Delete the GameMode
        /// </summary>
        /// <param name="id">GameMode Id</param>
        /// <returns>deleted GameMode object</returns>
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.GameMode))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.GameMode>> DeleteGameMode(Guid id)
        {
            var gameMode = await _bll.GameModes.FirstOrDefaultAsync(id);
            if (gameMode == null)
            {
                return NotFound(new V1DTO.MessageDTO("GameMode not found"));
            }

            await _bll.GameModes.RemoveAsync(gameMode);
            await _bll.SaveChangesAsync();

            return Ok(gameMode);
        }
    }
}