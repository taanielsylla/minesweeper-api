using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using PublicApi.DTO.v1.Mappers;
using V1DTO = PublicApi.DTO.v1;

namespace WebApp.ApiControllers._1._0
{
    /// <summary>
    /// Transactions - Stores user related deposit and withdraw data. 
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class TransactionsController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly TransactionMapper _mapper = new TransactionMapper();

        /// <summary>
        /// Constructor
        /// </summary>
        public TransactionsController(IAppBLL bll)
        {
            _bll = bll;
        }

        /// <summary>
        /// Get the Transaction collection.
        /// All records from the Transaction table
        /// </summary>
        /// <returns>List of all transactions</returns>
        [HttpGet]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<V1DTO.Transaction>))]
        public async Task<ActionResult<IEnumerable<V1DTO.Transaction>>> GetTransactions()
        {
            return Ok((await _bll.Transactions.GetAllAsync()).Select(e => _mapper.Map(e)));
        }


        /// <summary>
        /// Get single Transaction
        /// </summary>
        /// <param name="id">Transaction Id</param>
        /// <returns>request Transaction</returns>
        [HttpGet("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.Transaction))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.Transaction>> GetTransaction(Guid id)
        {
            var transaction = await _bll.Transactions.FirstOrDefaultAsync(id);

            if (transaction == null)
            {
                return NotFound(new V1DTO.MessageDTO("Transaction not found"));
            }

            return Ok(_mapper.Map(transaction));
        }

        /// <summary>
        /// Update the Transaction
        /// </summary>
        /// <param name="id">Transaction id</param>
        /// <param name="transaction">Transaction object</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(V1DTO.MessageDTO))]
        public async Task<IActionResult> PutTransaction(Guid id, V1DTO.Transaction transaction)
        {
            if (id != transaction.Id)
            {
                return BadRequest(new V1DTO.MessageDTO("id and transaction.id do not match"));
            }

            await _bll.Transactions.UpdateAsync(_mapper.Map(transaction));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Create a new Transaction
        /// </summary>
        /// <param name="transaction">Transaction object</param>
        /// <returns>created Transaction object</returns>
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(V1DTO.Transaction))]
        public async Task<ActionResult<V1DTO.Transaction>> PostTransaction(V1DTO.Transaction transaction)
        {
            var bllEntity = _mapper.Map(transaction);
            _bll.Transactions.Add(bllEntity);
            await _bll.SaveChangesAsync();
            transaction.Id = bllEntity.Id;

            return CreatedAtAction("GetTransaction",
                new {id = transaction.Id, version = HttpContext.GetRequestedApiVersion()?.ToString() ?? "0"},
                transaction);
        }

        /// <summary>
        /// Delete the Transaction
        /// </summary>
        /// <param name="id">Transaction Id</param>
        /// <returns>deleted Transaction object</returns>
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.Transaction))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.Transaction>> DeleteTransaction(Guid id)
        {
            var transaction = await _bll.Transactions.FirstOrDefaultAsync(id);
            if (transaction == null)
            {
                return NotFound(new V1DTO.MessageDTO("Transaction not found"));
            }

            await _bll.Transactions.RemoveAsync(transaction);
            await _bll.SaveChangesAsync();

            return Ok(transaction);
        }
    }
}