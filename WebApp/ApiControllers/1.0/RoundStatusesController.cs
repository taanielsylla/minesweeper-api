using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using PublicApi.DTO.v1.Mappers;
using V1DTO = PublicApi.DTO.v1;

namespace WebApp.ApiControllers._1._0
{
    /// <summary>
    /// RoundStatuses - NO_GAME, ACTIVE, RESOLVED.
    /// NO_GAME - No game has been started, should initialize new round.
    /// ACTIVE - Game round active, can bet and resolve round.
    /// RESOLVED - user either lost the bet or withdraw'ed money from some level in the game.
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class RoundStatusesController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly RoundStatusMapper _mapper = new RoundStatusMapper();

        /// <summary>
        /// Constructor
        /// </summary>
        public RoundStatusesController(IAppBLL bll)
        {
            _bll = bll;
        }

        /// <summary>
        /// Get the RoundStatus collection.
        /// All records for RoundStatuses
        /// </summary>
        /// <returns>List of all roundStatuses</returns>
        [HttpGet]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<V1DTO.RoundStatus>))]
        public async Task<ActionResult<IEnumerable<V1DTO.RoundStatus>>> GetRoundStatuses()
        {
            return Ok((await _bll.RoundStatuses.GetAllAsync()).Select(e => _mapper.Map(e)));
        }


        /// <summary>
        /// Get single RoundStatus
        /// </summary>
        /// <param name="id">RoundStatus Id</param>
        /// <returns>request RoundStatus</returns>
        [HttpGet("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.RoundStatus))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.RoundStatus>> GetRoundStatus(Guid id)
        {
            var roundStatus = await _bll.RoundStatuses.FirstOrDefaultAsync(id);

            if (roundStatus == null)
            {
                return NotFound(new V1DTO.MessageDTO("RoundStatus not found"));
            }

            return Ok(_mapper.Map(roundStatus));
        }

        /// <summary>
        /// Update the RoundStatus
        /// </summary>
        /// <param name="id">RoundStatus id</param>
        /// <param name="roundStatus">RoundStatus object</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(V1DTO.MessageDTO))]
        public async Task<IActionResult> PutRoundStatus(Guid id, V1DTO.RoundStatus roundStatus)
        {
            if (id != roundStatus.Id)
            {
                return BadRequest(new V1DTO.MessageDTO("id and roundStatus.id do not match"));
            }

            await _bll.RoundStatuses.UpdateAsync(_mapper.Map(roundStatus));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Create a new RoundStatus
        /// </summary>
        /// <param name="roundStatus">RoundStatus object</param>
        /// <returns>created RoundStatus object</returns>
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(V1DTO.RoundStatus))]
        public async Task<ActionResult<V1DTO.RoundStatus>> PostRoundStatus(V1DTO.RoundStatus roundStatus)
        {
            var bllEntity = _mapper.Map(roundStatus);
            _bll.RoundStatuses.Add(bllEntity);
            await _bll.SaveChangesAsync();
            roundStatus.Id = bllEntity.Id;

            return CreatedAtAction("GetRoundStatus",
                new {id = roundStatus.Id, version = HttpContext.GetRequestedApiVersion()?.ToString() ?? "0"},
                roundStatus);
        }

        /// <summary>
        /// Delete the RoundStatus
        /// </summary>
        /// <param name="id">RoundStatus Id</param>
        /// <returns>deleted RoundStatus object</returns>
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.RoundStatus))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.RoundStatus>> DeleteRoundStatus(Guid id)
        {
            var roundStatus = await _bll.RoundStatuses.FirstOrDefaultAsync(id);
            if (roundStatus == null)
            {
                return NotFound(new V1DTO.MessageDTO("RoundStatus not found"));
            }

            await _bll.RoundStatuses.RemoveAsync(roundStatus);
            await _bll.SaveChangesAsync();

            return Ok(roundStatus);
        }
    }
}