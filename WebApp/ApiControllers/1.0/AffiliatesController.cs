using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using PublicApi.DTO.v1.Mappers;
using V1DTO = PublicApi.DTO.v1;

namespace WebApp.ApiControllers._1._0
{
    /// <summary>
    /// Affiliates - Stores record of referrals, to see who has referred who to the site. 
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class AffiliatesController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly AffiliateMapper _mapper = new AffiliateMapper();

        /// <summary>
        /// Constructor
        /// </summary>
        public AffiliatesController(IAppBLL bll)
        {
            _bll = bll;
        }

        /// <summary>
        /// Get the predefined Affiliate collection.
        /// All records of referrals, for affiliate program
        /// </summary>
        /// <returns>List of all affiliates</returns>
        [HttpGet]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<V1DTO.Affiliate>))]
        public async Task<ActionResult<IEnumerable<V1DTO.Affiliate>>> GetAffiliates()
        {
            return Ok((await _bll.Affiliates.GetAllAsync()).Select(e => _mapper.Map(e)));
        }


        /// <summary>
        /// Get single Affiliate
        /// </summary>
        /// <param name="id">Affiliate Id</param>
        /// <returns>request Affiliate</returns>
        [HttpGet("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.Affiliate))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.Affiliate>> GetAffiliate(Guid id)
        {
            var affiliate = await _bll.Affiliates.FirstOrDefaultAsync(id);

            if (affiliate == null)
            {
                return NotFound(new V1DTO.MessageDTO("Affiliate not found"));
            }

            return Ok(_mapper.Map(affiliate));
        }

        /// <summary>
        /// Update the Affiliate
        /// </summary>
        /// <param name="id">Affiliate id</param>
        /// <param name="affiliate">Affiliate object</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(V1DTO.MessageDTO))]
        public async Task<IActionResult> PutAffiliate(Guid id, V1DTO.Affiliate affiliate)
        {
            if (id != affiliate.Id)
            {
                return BadRequest(new V1DTO.MessageDTO("id and affiliate.id do not match"));
            }

            await _bll.Affiliates.UpdateAsync(_mapper.Map(affiliate));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Create a new Affiliate
        /// </summary>
        /// <param name="affiliate">Affiliate object</param>
        /// <returns>created Affiliate object</returns>
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(V1DTO.Affiliate))]
        public async Task<ActionResult<V1DTO.Affiliate>> PostAffiliate(V1DTO.Affiliate affiliate)
        {
            var bllEntity = _mapper.Map(affiliate);
            _bll.Affiliates.Add(bllEntity);
            await _bll.SaveChangesAsync();
            affiliate.Id = bllEntity.Id;

            return CreatedAtAction("GetAffiliate",
                new {id = affiliate.Id, version = HttpContext.GetRequestedApiVersion()?.ToString() ?? "0"},
                affiliate);
        }

        /// <summary>
        /// Delete the Affiliate
        /// </summary>
        /// <param name="id">Affiliate Id</param>
        /// <returns>deleted Affiliate object</returns>
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.Affiliate))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.Affiliate>> DeleteAffiliate(Guid id)
        {
            var affiliate = await _bll.Affiliates.FirstOrDefaultAsync(id);
            if (affiliate == null)
            {
                return NotFound(new V1DTO.MessageDTO("Affiliate not found"));
            }

            await _bll.Affiliates.RemoveAsync(affiliate);
            await _bll.SaveChangesAsync();

            return Ok(_mapper.Map(affiliate));
        }
    }
}