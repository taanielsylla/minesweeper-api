using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using PublicApi.DTO.v1.Mappers;
using V1DTO = PublicApi.DTO.v1;

namespace WebApp.ApiControllers._1._0
{
    /// <summary>
    /// TransactionTypes - Stores possible types of a transaction.
    /// Possible values - WITHDRAW, DEPOSIT etc  
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class TransactionTypesController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly TransactionTypeMapper _mapper = new TransactionTypeMapper();

        /// <summary>
        /// Constructor
        /// </summary>
        public TransactionTypesController(IAppBLL bll)
        {
            _bll = bll;
        }

        /// <summary>
        /// Get the predefined TransactionType collection.
        /// All records from TransactionTypes table
        /// </summary>
        /// <returns>List of all transactionTypes</returns>
        [HttpGet]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<V1DTO.TransactionType>))]
        public async Task<ActionResult<IEnumerable<V1DTO.TransactionType>>> GetTransactionTypes()
        {
            return Ok((await _bll.TransactionTypes.GetAllAsync()).Select(e => _mapper.Map(e)));
        }


        /// <summary>
        /// Get single TransactionType
        /// </summary>
        /// <param name="id">TransactionType Id</param>
        /// <returns>request TransactionType</returns>
        [HttpGet("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.TransactionType))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.TransactionType>> GetTransactionType(Guid id)
        {
            var transactionType = await _bll.TransactionTypes.FirstOrDefaultAsync(id);

            if (transactionType == null)
            {
                return NotFound(new V1DTO.MessageDTO("TransactionType not found"));
            }

            return Ok(_mapper.Map(transactionType));
        }

        /// <summary>
        /// Update the TransactionType
        /// </summary>
        /// <param name="id">TransactionType id</param>
        /// <param name="transactionType">TransactionType object</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(V1DTO.MessageDTO))]
        public async Task<IActionResult> PutTransactionType(Guid id, V1DTO.TransactionType transactionType)
        {
            if (id != transactionType.Id)
            {
                return BadRequest(new V1DTO.MessageDTO("id and transactionType.id do not match"));
            }

            await _bll.TransactionTypes.UpdateAsync(_mapper.Map(transactionType));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Create a new TransactionType
        /// </summary>
        /// <param name="transactionType">TransactionType object</param>
        /// <returns>created TransactionType object</returns>
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(V1DTO.TransactionType))]
        public async Task<ActionResult<V1DTO.TransactionType>> PostTransactionType(
            V1DTO.TransactionType transactionType)
        {
            var bllEntity = _mapper.Map(transactionType);
            _bll.TransactionTypes.Add(bllEntity);
            await _bll.SaveChangesAsync();
            transactionType.Id = bllEntity.Id;

            return CreatedAtAction("GetTransactionType",
                new {id = transactionType.Id, version = HttpContext.GetRequestedApiVersion()?.ToString() ?? "0"},
                transactionType);
        }

        /// <summary>
        /// Delete the TransactionType
        /// </summary>
        /// <param name="id">TransactionType Id</param>
        /// <returns>deleted TransactionType object</returns>
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.TransactionType))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.TransactionType>> DeleteTransactionType(Guid id)
        {
            var transactionType = await _bll.TransactionTypes.FirstOrDefaultAsync(id);
            if (transactionType == null)
            {
                return NotFound(new V1DTO.MessageDTO("TransactionType not found"));
            }

            await _bll.TransactionTypes.RemoveAsync(transactionType);
            await _bll.SaveChangesAsync();

            return Ok(transactionType);
        }
    }
}