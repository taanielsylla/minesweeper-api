using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using PublicApi.DTO.v1.Mappers;
using V1DTO = PublicApi.DTO.v1;

namespace WebApp.ApiControllers._1._0
{
    /// <summary>
    /// Wallets - Stores user balance data for different currencies. 
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class WalletsController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly WalletMapper _mapper = new WalletMapper();

        /// <summary>
        /// Constructor
        /// </summary>
        public WalletsController(IAppBLL bll)
        {
            _bll = bll;
        }

        /// <summary>
        /// Get the Wallet collection.
        /// All records from Wallets
        /// </summary>
        /// <returns>List of all wallets</returns>
        [HttpGet]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<V1DTO.Wallet>))]
        public async Task<ActionResult<IEnumerable<V1DTO.Wallet>>> GetWallets()
        {
            return Ok((await _bll.Wallets.GetAllAsync()).Select(e => _mapper.Map(e)));
        }


        /// <summary>
        /// Get single Wallet
        /// </summary>
        /// <param name="id">Wallet Id</param>
        /// <returns>request Wallet</returns>
        [HttpGet("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.Wallet))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.Wallet>> GetWallet(Guid id)
        {
            var wallet = await _bll.Wallets.FirstOrDefaultAsync(id);

            if (wallet == null)
            {
                return NotFound(new V1DTO.MessageDTO("Wallet not found"));
            }

            return Ok(_mapper.Map(wallet));
        }

        /// <summary>
        /// Update the Wallet
        /// </summary>
        /// <param name="id">Wallet id</param>
        /// <param name="wallet">Wallet object</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(V1DTO.MessageDTO))]
        public async Task<IActionResult> PutWallet(Guid id, V1DTO.Wallet wallet)
        {
            if (id != wallet.Id)
            {
                return BadRequest(new V1DTO.MessageDTO("id and wallet.id do not match"));
            }

            await _bll.Wallets.UpdateAsync(_mapper.Map(wallet));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Create a new Wallet
        /// </summary>
        /// <param name="wallet">Wallet object</param>
        /// <returns>created Wallet object</returns>
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(V1DTO.Wallet))]
        public async Task<ActionResult<V1DTO.Wallet>> PostWallet(V1DTO.Wallet wallet)
        {
            var bllEntity = _mapper.Map(wallet);
            _bll.Wallets.Add(bllEntity);
            await _bll.SaveChangesAsync();
            wallet.Id = bllEntity.Id;

            return CreatedAtAction("GetWallet",
                new {id = wallet.Id, version = HttpContext.GetRequestedApiVersion()?.ToString() ?? "0"},
                wallet);
        }

        /// <summary>
        /// Delete the Wallet
        /// </summary>
        /// <param name="id">Wallet Id</param>
        /// <returns>deleted Wallet object</returns>
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.Wallet))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.Wallet>> DeleteWallet(Guid id)
        {
            var wallet = await _bll.Wallets.FirstOrDefaultAsync(id);
            if (wallet == null)
            {
                return NotFound(new V1DTO.MessageDTO("Wallet not found"));
            }

            await _bll.Wallets.RemoveAsync(wallet);
            await _bll.SaveChangesAsync();

            return Ok(wallet);
        }
    }
}